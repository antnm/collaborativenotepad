# CollaborativeNotepad

CollaborativeNotepad is a program made for two users to edit the same file simultaneously over the network.

![screenshot](Screenshot.png)

## Dependencies

- POSIX threads
- SQLite3
- GTK3
- GtkSourceView 4
- Meson and Ninja (for building)

## Building

```sh
meson build
ninja -C build
```
