#include "shared.h"
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

void disconnect(shared_data_t *data) {
  pthread_mutex_lock(data->mutex);
  if (data->connected) {
    shutdown(data->sd, SHUT_RDWR);
    close(data->sd);
    data->connected = false;
    data->should_clear = true;
  }
  pthread_mutex_unlock(data->mutex);
}

void reconnect(shared_data_t *data, const char *address, uint16_t port) {
  pthread_mutex_lock(data->mutex);
  shutdown(data->sd, SHUT_RDWR);
  close(data->sd);
  data->connected = false;
  data->should_clear = true;
  data->address = realloc(data->address, strlen(address) + 1);
  strcpy(data->address, address);
  data->port = port;
  pthread_mutex_unlock(data->mutex);
}

bool connected(shared_data_t *data) {
  pthread_mutex_lock(data->mutex);
  bool result = data->connected;
  pthread_mutex_unlock(data->mutex);
  return result;
}
