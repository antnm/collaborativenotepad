#ifndef SHARED_H
#define SHARED_H

#include <pthread.h>
#include <stdbool.h>
#include <stdint.h>

typedef struct {
  bool connected;
  bool quit;
  uint32_t id;
  char *address;
  uint16_t port;
  int sd;
  pthread_mutex_t *mutex;
  bool should_clear;
} shared_data_t;

void disconnect(shared_data_t *data);

void reconnect(shared_data_t *data, const char *address, uint16_t port);

bool connected(shared_data_t *data);

#endif // SHARED_H
