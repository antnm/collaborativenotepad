#include "editor_buffer.h"
#include "../message.h"
#include "shared.h"
#include "widgets/editor.h"
#include <sys/socket.h>

struct _CollaborativeNotepadEditorBuffer {
  GtkSourceBuffer parent_instance;
  shared_data_t *shared;
  bool manual_insert;
  bool manual_delete;
  bool waiting_insert_confirmation;
  bool waiting_delete_confirmation;
  GtkTextTag *other_tag;
  GtkTextMark *other_mark;
  bool clearing;
  GtkSourceLanguageManager *language_manager;
};

G_DEFINE_TYPE(CollaborativeNotepadEditorBuffer,
              collaborative_notepad_editor_buffer, GTK_SOURCE_TYPE_BUFFER)

void collaborative_notepad_editor_buffer_init(
    CollaborativeNotepadEditorBuffer *editor_buffer) {}

void collaborative_notepad_editor_buffer_class_init(
    CollaborativeNotepadEditorBufferClass *class) {}

void send_position(CollaborativeNotepadEditorBuffer *editor_buffer) {
  if (!connected(editor_buffer->shared))
    return;

  request req;
  req.type = REQUEST_CHANGE_POSITION;

  GtkTextIter insert;
  gtk_text_buffer_get_iter_at_mark(
      GTK_TEXT_BUFFER(editor_buffer), &insert,
      gtk_text_buffer_get_insert(GTK_TEXT_BUFFER(editor_buffer)));
  req.pos.column = gtk_text_iter_get_line_index(&insert);
  req.pos.line = gtk_text_iter_get_line(&insert);

  GtkTextIter selection;
  gtk_text_buffer_get_iter_at_mark(
      GTK_TEXT_BUFFER(editor_buffer), &selection,
      gtk_text_buffer_get_selection_bound(GTK_TEXT_BUFFER(editor_buffer)));
  req.pos_end.column = gtk_text_iter_get_line_index(&selection);
  req.pos_end.line = gtk_text_iter_get_line(&selection);

  if (!send_request(req, editor_buffer->shared->sd))
    disconnect(editor_buffer->shared);
}

void insert_text_override(CollaborativeNotepadEditorBuffer *editor_buffer,
                          GtkTextIter *location, char *text, int len,
                          gpointer user_data) {
  if (editor_buffer->manual_insert) {
    editor_buffer->manual_insert = false;
    g_signal_chain_from_overridden_handler(editor_buffer, location, text, len,
                                           user_data);
    send_position(editor_buffer);
  } else if (!editor_buffer->waiting_delete_confirmation) {
    editor_buffer->waiting_insert_confirmation = true;
    if (!connected(editor_buffer->shared))
      return;

    request req;
    req.type = REQUEST_INSERT;
    req.pos.line = gtk_text_iter_get_line(location);
    req.pos.column = gtk_text_iter_get_line_index(location);
    req.str.size = len + 1;
    req.str.data = text;

    if (!send_request(req, editor_buffer->shared->sd))
      disconnect(editor_buffer->shared);
  }
}

void delete_range_override(CollaborativeNotepadEditorBuffer *editor_buffer,
                           GtkTextIter *start, GtkTextIter *end,
                           gpointer user_data) {
  if (editor_buffer->manual_delete) {
    editor_buffer->manual_delete = false;
    g_signal_chain_from_overridden_handler(editor_buffer, start, end,
                                           user_data);
    send_position(editor_buffer);
  } else if (editor_buffer->clearing) {
    editor_buffer->clearing = false;
    g_signal_chain_from_overridden_handler(editor_buffer, start, end,
                                           user_data);
  } else if (!editor_buffer->waiting_delete_confirmation) {
    editor_buffer->waiting_delete_confirmation = true;
    if (!connected(editor_buffer->shared))
      return;

    request req;
    req.type = REQUEST_DELETE_RANGE;
    req.pos.column = gtk_text_iter_get_line_index(start);
    req.pos.line = gtk_text_iter_get_line(start);
    req.pos_end.column = gtk_text_iter_get_line_index(end);
    req.pos_end.line = gtk_text_iter_get_line(end);

    if (!send_request(req, editor_buffer->shared->sd))
      disconnect(editor_buffer->shared);
  }
}

void mark_set_override(CollaborativeNotepadEditorBuffer *editor_buffer,
                       GtkTextIter *location, GtkTextMark *mark,
                       gpointer user_data) {
  g_signal_chain_from_overridden_handler(editor_buffer, location, mark,
                                         user_data);
  if (mark == gtk_text_buffer_get_insert(GTK_TEXT_BUFFER(editor_buffer))) {
    send_position(editor_buffer);
  }
}

void collaborative_notepad_editor_buffer_insert(
    CollaborativeNotepadEditorBuffer *editor_buffer, bool self, position pos,
    string str) {
  if (self) {
    editor_buffer->waiting_insert_confirmation = false;
  }
  editor_buffer->manual_insert = true;
  GtkTextIter location;
  gtk_text_buffer_get_iter_at_line_index(GTK_TEXT_BUFFER(editor_buffer),
                                         &location, pos.line, pos.column);
  g_signal_emit_by_name(editor_buffer, "insert-text", &location, str.data,
                        str.size - 1, NULL);
}

void collaborative_notepad_editor_buffer_delete_range(
    CollaborativeNotepadEditorBuffer *editor_buffer, bool self, position pos,
    position pos_end) {
  if (self) {
    editor_buffer->waiting_delete_confirmation = false;
  }
  editor_buffer->manual_delete = true;
  GtkTextIter start;
  gtk_text_buffer_get_iter_at_line_index(GTK_TEXT_BUFFER(editor_buffer), &start,
                                         pos.line, pos.column);
  GtkTextIter end;
  gtk_text_buffer_get_iter_at_line_index(GTK_TEXT_BUFFER(editor_buffer), &end,
                                         pos_end.line, pos_end.column);
  g_signal_emit_by_name(editor_buffer, "delete-range", &start, &end, NULL);
}

void collaborative_notepad_editor_buffer_change_position_other(
    CollaborativeNotepadEditorBuffer *editor_buffer, position pos,
    position pos_end) {
  GtkTextIter start, end;
  gtk_text_buffer_get_start_iter(GTK_TEXT_BUFFER(editor_buffer), &start);
  gtk_text_buffer_get_end_iter(GTK_TEXT_BUFFER(editor_buffer), &end);

  gtk_text_buffer_remove_tag(GTK_TEXT_BUFFER(editor_buffer),
                             editor_buffer->other_tag, &start, &end);

  GtkTextIter iter, iter_end;
  gtk_text_buffer_get_iter_at_line_index(GTK_TEXT_BUFFER(editor_buffer), &iter,
                                         pos.line, pos.column);
  gtk_text_buffer_get_iter_at_line_index(
      GTK_TEXT_BUFFER(editor_buffer), &iter_end, pos_end.line, pos_end.column);

  gtk_text_buffer_apply_tag(GTK_TEXT_BUFFER(editor_buffer),
                            editor_buffer->other_tag, &iter, &iter_end);

  gtk_text_buffer_move_mark(GTK_TEXT_BUFFER(editor_buffer),
                            editor_buffer->other_mark, &iter);
  gtk_text_mark_set_visible(editor_buffer->other_mark, true);
}

void collaborative_notepad_editor_buffer_remove_other(
    CollaborativeNotepadEditorBuffer *editor_buffer) {
  gtk_text_mark_set_visible(editor_buffer->other_mark, false);

  GtkTextIter start, end;
  gtk_text_buffer_get_start_iter(GTK_TEXT_BUFFER(editor_buffer), &start);
  gtk_text_buffer_get_end_iter(GTK_TEXT_BUFFER(editor_buffer), &end);
  gtk_text_buffer_remove_tag(GTK_TEXT_BUFFER(editor_buffer),
                             editor_buffer->other_tag, &start, &end);
}

void collaborative_notepad_editor_buffer_clear(
    CollaborativeNotepadEditorBuffer *editor_buffer) {
  GtkTextIter start, end;
  editor_buffer->clearing = true;
  gtk_text_buffer_get_start_iter(GTK_TEXT_BUFFER(editor_buffer), &start);
  gtk_text_buffer_get_end_iter(GTK_TEXT_BUFFER(editor_buffer), &end);
  if (gtk_text_iter_compare(&start, &end) != 0)
    g_signal_emit_by_name(editor_buffer, "delete-range", &start, &end, NULL);

  gtk_text_mark_set_visible(editor_buffer->other_mark, false);
  gtk_text_buffer_remove_tag(GTK_TEXT_BUFFER(editor_buffer),
                             editor_buffer->other_tag, &start, &end);
}

GtkListStore *collaborative_notepad_editor_get_available_languages(
    CollaborativeNotepadEditorBuffer *editor_buffer) {
  const gchar *const *languages = gtk_source_language_manager_get_language_ids(
      editor_buffer->language_manager);
  GtkListStore *result = gtk_list_store_new(2, G_TYPE_STRING, G_TYPE_STRING);
  gtk_list_store_insert_with_values(result, NULL, -1, 0, "", 1, "Plain Text",
                                    -1);
  for (size_t i = 0; languages[i] != NULL; ++i) {
    GtkSourceLanguage *language = gtk_source_language_manager_get_language(
        editor_buffer->language_manager, languages[i]);
    gtk_list_store_insert_with_values(
        result, NULL, -1, 0, gtk_source_language_get_id(language), 1,
        gtk_source_language_get_name(language), -1);
  }
  return result;
}

void collaborative_notepad_editor_buffer_set_language(
    CollaborativeNotepadEditorBuffer *editor_buffer, char *language) {
  gtk_source_buffer_set_language(
      GTK_SOURCE_BUFFER(editor_buffer),
      gtk_source_language_manager_get_language(editor_buffer->language_manager,
                                               language));
}

CollaborativeNotepadEditorBuffer *
collaborative_notepad_editor_buffer_new(shared_data_t *shared) {
  CollaborativeNotepadEditorBuffer *result =
      g_object_new(COLLABORATIVE_NOTEPAD_EDITOR_BUFFER_TYPE, NULL);
  gtk_source_undo_manager_begin_not_undoable_action(
      gtk_source_buffer_get_undo_manager(GTK_SOURCE_BUFFER(result)));
  result->shared = shared;
  result->manual_insert = false;
  result->manual_delete = false;
  result->waiting_insert_confirmation = false;
  result->waiting_delete_confirmation = false;
  result->clearing = false;

  g_signal_override_class_handler("insert-text",
                                  COLLABORATIVE_NOTEPAD_EDITOR_BUFFER_TYPE,
                                  G_CALLBACK(insert_text_override));
  g_signal_override_class_handler("delete-range",
                                  COLLABORATIVE_NOTEPAD_EDITOR_BUFFER_TYPE,
                                  G_CALLBACK(delete_range_override));
  g_signal_override_class_handler("mark-set",
                                  COLLABORATIVE_NOTEPAD_EDITOR_BUFFER_TYPE,
                                  G_CALLBACK(mark_set_override));

  result->other_tag = gtk_text_tag_new(NULL);
  GdkRGBA rgba;
  gdk_rgba_parse(&rgba, "rgb(246,97,81)");
  GValue rgba_val = G_VALUE_INIT;
  g_value_init(&rgba_val, GDK_TYPE_RGBA);
  g_value_set_boxed(&rgba_val, &rgba);
  g_object_set_property(G_OBJECT(result->other_tag), "background-rgba",
                        &rgba_val);
  gtk_text_tag_table_add(gtk_text_buffer_get_tag_table(GTK_TEXT_BUFFER(result)),
                         result->other_tag);

  GtkTextIter start_iter;
  gtk_text_buffer_get_start_iter(GTK_TEXT_BUFFER(result), &start_iter);
  result->other_mark = gtk_text_buffer_create_mark(GTK_TEXT_BUFFER(result),
                                                   NULL, &start_iter, false);

  result->language_manager = gtk_source_language_manager_new();

  return result;
}
