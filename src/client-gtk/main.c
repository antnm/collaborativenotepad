#include "../message.h"
#include "editor_buffer.h"
#include "file_list_store.h"
#include "responses.h"
#include "widgets/editor.h"
#include "widgets/notification.h"
#include "window.h"
#include <arpa/inet.h>
#include <fcntl.h>
#include <gtk/gtk.h>
#include <pthread.h>

typedef struct {
  process_responses_data_t *process_data;
  recieve_responses_data_t *recieve_data;
  guint process_responses_tag;
  pthread_t thread;
} shutdown_data_t;

void application_shutdown_cb(GtkApplication *app, shutdown_data_t *data) {
  pthread_mutex_lock(data->process_data->shared->mutex);
  data->process_data->shared->quit = true;
  pthread_mutex_unlock(data->process_data->shared->mutex);
  disconnect(data->process_data->shared);
  pthread_join(data->thread, NULL);
  g_source_remove(data->process_responses_tag);
  pthread_mutex_destroy(data->process_data->shared->mutex);
  free(data->process_data->shared->mutex);
  free(data->process_data);
  free(data->recieve_data);
  free(data);
}

void activate(GtkApplication *app, gpointer data) {
  shared_data_t *shared = malloc(sizeof(shared_data_t));
  shared->connected = false;
  shared->should_clear = false;
  shared->quit = false;
  shared->address = malloc(10);
  strcpy(shared->address, "127.0.0.1");
  shared->port = 2908;

  int pipefd[2];
  pipe(pipefd);

  recieve_responses_data_t *recieve_data =
      malloc(sizeof(recieve_responses_data_t));
  recieve_data->shared = shared;
  recieve_data->shared->mutex = malloc(sizeof(pthread_mutex_t));
  pthread_mutex_init(recieve_data->shared->mutex, NULL);
  recieve_data->pipefd = pipefd[1];

  process_responses_data_t *process_data =
      malloc(sizeof(process_responses_data_t));
  process_data->shared = shared;
  process_data->shared->mutex = recieve_data->shared->mutex;
  process_data->list_store =
      gtk_list_store_new(3, G_TYPE_STRING, G_TYPE_UINT, G_TYPE_STRING);
  process_data->editor_buffer = collaborative_notepad_editor_buffer_new(shared);
  process_data->language_store =
      collaborative_notepad_editor_get_available_languages(
          process_data->editor_buffer);
  process_data->window = collaborative_notepad_window_new(
      process_data->list_store, process_data->language_store,
      process_data->editor_buffer, shared);
  gtk_window_set_application(GTK_WINDOW(process_data->window), app);
  process_data->opened_file = NULL;
  process_data->pipefd = pipefd[0];
  fcntl(pipefd[0], F_SETFL, O_NONBLOCK);

  shutdown_data_t *shutdown_data = malloc(sizeof(shutdown_data_t));
  shutdown_data->process_data = process_data;
  shutdown_data->recieve_data = recieve_data;
  shutdown_data->process_responses_tag = gdk_threads_add_timeout_full(
      G_PRIORITY_HIGH_IDLE, 20, &process_responses, process_data, NULL);

  pthread_create(&shutdown_data->thread, NULL, &recieve_responses,
                 recieve_data);

  g_signal_connect(app, "shutdown", G_CALLBACK(application_shutdown_cb),
                   shutdown_data);

  gtk_widget_show_all(GTK_WIDGET(process_data->window));
}

int main(int argc, char **argv) {
  GtkApplication *app = gtk_application_new(NULL, G_APPLICATION_FLAGS_NONE);

  g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);

  int status = g_application_run(G_APPLICATION(app), argc, argv);
  g_object_unref(app);

  return status;
}
