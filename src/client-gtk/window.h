#ifndef WINDOW_H
#define WINDOW_H

#include "editor_buffer.h"
#include "shared.h"
#include <gtk/gtk.h>
#include <gtksourceview/gtksource.h>
#include <stdbool.h>

G_BEGIN_DECLS

#define COLLABORATIVE_NOTEPAD_WINDOW_TYPE                                      \
  collaborative_notepad_window_get_type()
G_DECLARE_FINAL_TYPE(CollaborativeNotepadWindow, collaborative_notepad_window,
                     COLLABORATIVE_NOTEPAD, WINDOW, GtkWindow)

void collaborative_notepad_window_show_notification(
    CollaborativeNotepadWindow *window, char *message);
void collaborative_notepad_window_show_file_list(
    CollaborativeNotepadWindow *window);
void collaborative_notepad_window_show_editor(
    CollaborativeNotepadWindow *window);
void collaborative_notepad_window_scroll_to_cursor(
    CollaborativeNotepadWindow *window);
void collaborative_notepad_window_select_language(
    CollaborativeNotepadWindow *window, char *language);
CollaborativeNotepadWindow *collaborative_notepad_window_new(
    GtkListStore *list_store, GtkListStore *language_store,
    CollaborativeNotepadEditorBuffer *editor_buffer, shared_data_t *shared);

G_END_DECLS

#endif // WINDOW_H
