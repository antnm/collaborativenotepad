#ifndef EDITOR_BUFFER_H
#define EDITOR_BUFFER_H

#include "../message.h"
#include "shared.h"
#include <gtk/gtk.h>
#include <gtksourceview/gtksource.h>
#include <stdbool.h>

G_BEGIN_DECLS

#define COLLABORATIVE_NOTEPAD_EDITOR_BUFFER_TYPE                               \
  collaborative_notepad_editor_buffer_get_type()
G_DECLARE_FINAL_TYPE(CollaborativeNotepadEditorBuffer,
                     collaborative_notepad_editor_buffer, COLLABORATIVE_NOTEPAD,
                     EDITOR_BUFFER, GtkSourceBuffer)

void collaborative_notepad_editor_buffer_insert(
    CollaborativeNotepadEditorBuffer *editor_buffer, bool self, position pos,
    string str);
void collaborative_notepad_editor_buffer_delete_range(
    CollaborativeNotepadEditorBuffer *editor_buffer, bool self, position pos,
    position pos_end);
void collaborative_notepad_editor_buffer_change_position_other(
    CollaborativeNotepadEditorBuffer *editor_buffer, position pos,
    position pos_end);
void collaborative_notepad_editor_buffer_remove_other(
    CollaborativeNotepadEditorBuffer *editor_buffer);
void collaborative_notepad_editor_buffer_clear(
    CollaborativeNotepadEditorBuffer *editor_buffer);
GtkListStore *collaborative_notepad_editor_get_available_languages(
    CollaborativeNotepadEditorBuffer *editor_buffer);
void collaborative_notepad_editor_buffer_set_language(
    CollaborativeNotepadEditorBuffer *editor_buffer, char *language);
CollaborativeNotepadEditorBuffer *
collaborative_notepad_editor_buffer_new(shared_data_t *shared);

G_END_DECLS

#endif // EDITOR_BUFFER_H
