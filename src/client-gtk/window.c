#include "window.h"
#include "../message.h"
#include "widgets/editor.h"
#include "widgets/file_list.h"
#include "widgets/header_bar.h"
#include "widgets/notification.h"

struct _CollaborativeNotepadWindow {
  GtkWindow parent_instance;
  CollaborativeNotepadHeaderBar *header_bar;
  CollaborativeNotepadNotification *notification;
  GtkWidget *stack;
  CollaborativeNotepadFileList *file_list;
  CollaborativeNotepadEditor *editor;
};

G_DEFINE_TYPE(CollaborativeNotepadWindow, collaborative_notepad_window,
              GTK_TYPE_WINDOW)

void collaborative_notepad_window_init(CollaborativeNotepadWindow *window) {}

void collaborative_notepad_window_class_init(
    CollaborativeNotepadWindowClass *class) {}

void collaborative_notepad_window_show_notification(
    CollaborativeNotepadWindow *window, char *message) {
  collaborative_notepad_notification_show(window->notification, message);
}

void collaborative_notepad_window_show_file_list(
    CollaborativeNotepadWindow *window) {
  gtk_stack_set_visible_child(GTK_STACK(window->stack),
                              GTK_WIDGET(window->file_list));
  collaborative_notepad_header_bar_show_file_list(window->header_bar);
}

void collaborative_notepad_window_show_editor(
    CollaborativeNotepadWindow *window) {
  gtk_stack_set_visible_child(GTK_STACK(window->stack),
                              GTK_WIDGET(window->editor));
  collaborative_notepad_header_bar_show_editor(window->header_bar);
}

void collaborative_notepad_window_scroll_to_cursor(
    CollaborativeNotepadWindow *window) {
  collaborative_notepad_editor_scroll_to_cursor(window->editor);
}

void collaborative_notepad_window_select_language(
    CollaborativeNotepadWindow *window, char *language) {
  collaborative_notepad_header_bar_select_language(window->header_bar,
                                                   language);
}

CollaborativeNotepadWindow *collaborative_notepad_window_new(
    GtkListStore *list_store, GtkListStore *language_store,
    CollaborativeNotepadEditorBuffer *editor_buffer, shared_data_t *shared) {
  CollaborativeNotepadWindow *result =
      g_object_new(COLLABORATIVE_NOTEPAD_WINDOW_TYPE, NULL);
  gtk_window_set_default_size(GTK_WINDOW(result), 600, 400);

  result->header_bar =
      collaborative_notepad_header_bar_new(language_store, shared, editor_buffer);
  gtk_window_set_titlebar(GTK_WINDOW(result), GTK_WIDGET(result->header_bar));
  collaborative_notepad_header_bar_show_file_list(result->header_bar);

  GtkWidget *overlay = gtk_overlay_new();
  gtk_container_add(GTK_CONTAINER(result), overlay);

  result->notification = collaborative_notepad_notification_new();
  gtk_overlay_add_overlay(GTK_OVERLAY(overlay),
                          GTK_WIDGET(result->notification));

  result->stack = gtk_stack_new();
  gtk_container_add(GTK_CONTAINER(overlay), result->stack);
  gtk_stack_set_transition_type(GTK_STACK(result->stack),
                                GTK_STACK_TRANSITION_TYPE_SLIDE_LEFT_RIGHT);

  result->file_list = collaborative_notepad_file_list_new(list_store, shared);
  gtk_container_add(GTK_CONTAINER(result->stack),
                    GTK_WIDGET(result->file_list));

  result->editor = collaborative_notepad_editor_new(editor_buffer, shared);
  gtk_container_add(GTK_CONTAINER(result->stack), GTK_WIDGET(result->editor));

  return result;
}
