#ifndef PREFERENCES_H
#define PREFERENCES_H

#include <gtk/gtk.h>
#include <stdbool.h>
#include "../shared.h"

G_BEGIN_DECLS

#define COLLABORATIVE_NOTEPAD_PREFERENCES_TYPE                                 \
  collaborative_notepad_preferences_get_type()
G_DECLARE_FINAL_TYPE(CollaborativeNotepadPreferences,
                     collaborative_notepad_preferences, COLLABORATIVE_NOTEPAD,
                     PREFERENCES, GtkDialog)

void collaborative_notepad_preferences_show(
    CollaborativeNotepadPreferences *preferences);
CollaborativeNotepadPreferences *
collaborative_notepad_preferences_new(shared_data_t *shared);

G_END_DECLS

#endif // PREFERENCES_H
