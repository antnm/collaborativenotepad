#ifndef NOTIFICATION_H
#define NOTIFICATION_H

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define COLLABORATIVE_NOTEPAD_NOTIFICATION_TYPE                                \
  collaborative_notepad_notification_get_type()
G_DECLARE_FINAL_TYPE(CollaborativeNotepadNotification,
                     collaborative_notepad_notification, COLLABORATIVE_NOTEPAD,
                     NOTIFICATION, GtkRevealer)

CollaborativeNotepadNotification *collaborative_notepad_notification_new();
void collaborative_notepad_notification_show(CollaborativeNotepadNotification *notification, char *message);

G_END_DECLS

#endif // NOTIFICATION_H
