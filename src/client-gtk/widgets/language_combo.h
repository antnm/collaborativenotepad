#ifndef LANGUAGE_COMBO_H
#define LANGUAGE_COMBO_H

#include "../responses.h"
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define COLLABORATIVE_NOTEPAD_LANGUAGE_COMBO_TYPE                              \
  collaborative_notepad_language_combo_get_type()
G_DECLARE_FINAL_TYPE(CollaborativeNotepadLanguageCombo,
                     collaborative_notepad_language_combo,
                     COLLABORATIVE_NOTEPAD, LANGUAGE_COMBO, GtkComboBox)

void collaborative_notepad_language_combo_select_language(
    CollaborativeNotepadLanguageCombo *language_combo, char *language);
CollaborativeNotepadLanguageCombo *
collaborative_notepad_language_combo_new(GtkListStore *language_store,
                                         shared_data_t *shared);

G_END_DECLS

#endif // LANGUAGE_COMBO_H
