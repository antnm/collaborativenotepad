#include "file_list.h"
#include "../../message.h"
#include "../file_list_store.h"
#include "remove_button.h"

struct _CollaborativeNotepadFileList {
  GtkScrolledWindow parent_instance;
  GtkListStore *list_store;
  shared_data_t *shared;
};

G_DEFINE_TYPE(CollaborativeNotepadFileList, collaborative_notepad_file_list,
              GTK_TYPE_SCROLLED_WINDOW)

void collaborative_notepad_file_list_init(
    CollaborativeNotepadFileList *file_list) {}

void collaborative_notepad_file_list_class_init(
    CollaborativeNotepadFileListClass *class) {}

void row_activated(CollaborativeNotepadFileList *file_list, GtkTreePath *path,
                   GtkTreeViewColumn *column, GtkTreeView *tree_view) {
  if (!connected(file_list->shared))
    return;

  GtkTreeIter iter;
  gtk_tree_model_get_iter_first(GTK_TREE_MODEL(file_list->list_store), &iter);
  int position = *gtk_tree_path_get_indices(path);
  for (int i = 0; i < position; ++i)
    gtk_tree_model_iter_next(GTK_TREE_MODEL(file_list->list_store), &iter);

  GValue file_name = G_VALUE_INIT;
  gtk_tree_model_get_value(GTK_TREE_MODEL(file_list->list_store), &iter,
                           FILE_NAME, &file_name);

  request req;
  req.type = REQUEST_OPEN_FILE;
  req.str.size = strlen(g_value_get_string(&file_name)) + 1;
  req.str.data = malloc(req.str.size);
  strcpy(req.str.data, g_value_get_string(&file_name));

  if (!send_request(req, file_list->shared->sd))
    disconnect(file_list->shared);

  free_request(req);
  g_value_unset(&file_name);
}

CollaborativeNotepadFileList *
collaborative_notepad_file_list_new(GtkListStore *list_store, shared_data_t *shared) {
  CollaborativeNotepadFileList *result =
      g_object_new(COLLABORATIVE_NOTEPAD_FILE_LIST_TYPE, NULL);
  result->list_store = list_store;
  result->shared = shared;

  GtkWidget *tree_view = gtk_tree_view_new();
  gtk_container_add(GTK_CONTAINER(result), tree_view);
  gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(tree_view), FALSE);
  g_signal_connect_swapped(tree_view, "row-activated",
                           G_CALLBACK(row_activated), result);

  gtk_tree_view_set_model(GTK_TREE_VIEW(tree_view), GTK_TREE_MODEL(list_store));

  GtkTreeViewColumn *tree_view_column = gtk_tree_view_column_new();
  gtk_tree_view_append_column(GTK_TREE_VIEW(tree_view), tree_view_column);
  gtk_tree_view_column_set_sizing(tree_view_column, GTK_TREE_VIEW_COLUMN_AUTOSIZE);

  GtkCellRenderer *cell_name = gtk_cell_renderer_text_new();
  gtk_tree_view_column_pack_start(tree_view_column, cell_name, TRUE);
  gtk_tree_view_column_add_attribute(tree_view_column, cell_name, "text",
                                     FILE_NAME);
  gtk_cell_renderer_set_padding(cell_name, 16, 16);
  GValue ellipsize = G_VALUE_INIT;
  g_value_init(&ellipsize, PANGO_TYPE_ELLIPSIZE_MODE);
  g_value_set_enum(&ellipsize, PANGO_ELLIPSIZE_MIDDLE);
  g_object_set_property(G_OBJECT(cell_name), "ellipsize", &ellipsize);
  g_value_unset(&ellipsize);

  CollaborativeNotepadRemoveButton *cell_remove_button =
      collaborative_notepad_remove_button_new(list_store, shared);
  gtk_tree_view_column_pack_end(tree_view_column,
                                GTK_CELL_RENDERER(cell_remove_button), FALSE);
  gtk_cell_renderer_set_padding(GTK_CELL_RENDERER(cell_remove_button), 16, 16);

  GtkCellRenderer *cell_users = gtk_cell_renderer_text_new();
  gtk_tree_view_column_pack_end(tree_view_column, cell_users, FALSE);
  gtk_tree_view_column_add_attribute(tree_view_column, cell_users, "markup",
                                     USER_COUNT_MARKUP);
  gtk_cell_renderer_set_padding(cell_users, 16, 16);

  return result;
}
