#include <stdio.h>
#include "header_bar.h"
#include "../../message.h"
#include "gtk/gtkrevealer.h"
#include "language_combo.h"
#include "preferences.h"

struct _CollaborativeNotepadHeaderBar {
  GtkHeaderBar parent_instance;
  shared_data_t *shared;
  GtkWidget *button_stack;
  GtkWidget *add_button;
  GtkWidget *back_button;
  GtkWidget *entry;
  GtkWidget *add_popover;
  GtkWidget *menu_popover;
  CollaborativeNotepadPreferences *preferences;
  CollaborativeNotepadLanguageCombo *language_combo;
  GtkWidget *editor_actions_revealer;
  CollaborativeNotepadEditorBuffer *buffer;
};

G_DEFINE_TYPE(CollaborativeNotepadHeaderBar, collaborative_notepad_header_bar,
              GTK_TYPE_HEADER_BAR)

void show_add_popover_cb(CollaborativeNotepadHeaderBar *header_bar,
                         gpointer data) {
  gtk_widget_show_all(header_bar->add_popover);
}

void show_menu_popover_cb(CollaborativeNotepadHeaderBar *header_bar,
                          gpointer data) {
  gtk_widget_show_all(header_bar->menu_popover);
}

void add_file_cb(CollaborativeNotepadHeaderBar *header_bar, gpointer data) {
  if (!connected(header_bar->shared))
    return;

  request req;
  req.type = REQUEST_CREATE_FILE;
  req.str.size = strlen(gtk_entry_get_text(GTK_ENTRY(header_bar->entry))) + 1;
  req.str.data = malloc(req.str.size);
  strcpy(req.str.data, gtk_entry_get_text(GTK_ENTRY(header_bar->entry)));
  if (!send_request(req, header_bar->shared->sd))
    disconnect(header_bar->shared);

  free_request(req);

  gtk_popover_popdown(GTK_POPOVER(header_bar->add_popover));
  gtk_entry_set_text(GTK_ENTRY(header_bar->entry), "");
}

void close_file_cb(CollaborativeNotepadHeaderBar *header_bar, gpointer data) {
  if (!connected(header_bar->shared))
    return;

  request req;
  req.type = REQUEST_CLOSE_FILE;

  if (!send_request(req, header_bar->shared->sd))
    disconnect(header_bar->shared);

  free_request(req);
}

void show_about_cb(CollaborativeNotepadHeaderBar *header_bar, gpointer data) {
  gchar *authors[] = {"Antonio Mihăeș", NULL};
  gtk_show_about_dialog(NULL, "title", "About Collaborative Notepad",
                        "program-name", "Collaborative Notepad", "comments",
                        "Edit files collaboratively", "license-type",
                        GTK_LICENSE_GPL_3_0, "authors", authors, "copyright",
                        "© 2021 Antonio Mihăeș", "logo-icon-name", NULL, NULL);
}

void show_preferences_cb(CollaborativeNotepadHeaderBar *header_bar,
                         gpointer data) {
  collaborative_notepad_preferences_show(header_bar->preferences);
}

void save_cb(CollaborativeNotepadHeaderBar *header_bar,
             gpointer data) {
  GtkWidget *file_chooser =
      gtk_file_chooser_dialog_new("Save File",
                                  GTK_WINDOW(gtk_widget_get_parent(GTK_WIDGET(header_bar))),
                                  GTK_FILE_CHOOSER_ACTION_SAVE,
                                  "Cancel",
                                  GTK_RESPONSE_CANCEL,
                                  "Save",
                                  GTK_RESPONSE_ACCEPT,
                                  NULL);
  gtk_file_chooser_set_do_overwrite_confirmation (GTK_FILE_CHOOSER(file_chooser),
                                                  TRUE);
  for (;;) {
    gint result = gtk_dialog_run(GTK_DIALOG(file_chooser));
    if (result == GTK_RESPONSE_ACCEPT) {
      char *filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(file_chooser));
      GtkTextIter start, end;
      gtk_text_buffer_get_start_iter(GTK_TEXT_BUFFER(header_bar->buffer), &start);
      gtk_text_buffer_get_end_iter(GTK_TEXT_BUFFER(header_bar->buffer), &end);
      FILE *file = fopen(filename, "w");
      if (file == NULL) {
        g_free(filename);
        continue;
      }
      fprintf(file,
              "%s",
              gtk_text_buffer_get_text(GTK_TEXT_BUFFER(header_bar->buffer),
                                       &start,
                                       &end,
                                       FALSE));
      fclose(file);
      g_free(filename);
      break;
    } else {
      break;
    }
  }

  gtk_widget_destroy(file_chooser);
}

void collaborative_notepad_header_bar_show_file_list(
    CollaborativeNotepadHeaderBar *header_bar) {
  gtk_stack_set_visible_child(GTK_STACK(header_bar->button_stack),
                              header_bar->add_button);
  gtk_revealer_set_reveal_child(
      GTK_REVEALER(header_bar->editor_actions_revealer), FALSE);
}

void collaborative_notepad_header_bar_show_editor(
    CollaborativeNotepadHeaderBar *header_bar) {
  gtk_stack_set_visible_child(GTK_STACK(header_bar->button_stack),
                              header_bar->back_button);
  gtk_revealer_set_reveal_child(
      GTK_REVEALER(header_bar->editor_actions_revealer), TRUE);
}

void collaborative_notepad_header_bar_select_language(
    CollaborativeNotepadHeaderBar *header_bar, char *language) {
  collaborative_notepad_language_combo_select_language(
      header_bar->language_combo, language);
}

void collaborative_notepad_header_bar_init(
    CollaborativeNotepadHeaderBar *header_bar) {}

void collaborative_notepad_header_bar_class_init(
    CollaborativeNotepadHeaderBarClass *class) {}

CollaborativeNotepadHeaderBar *
collaborative_notepad_header_bar_new(GtkListStore *language_store,
                                     shared_data_t *shared,
                                     CollaborativeNotepadEditorBuffer *buffer) {
  CollaborativeNotepadHeaderBar *result =
      g_object_new(COLLABORATIVE_NOTEPAD_HEADER_BAR_TYPE, NULL);
  result->shared = shared;
  result->buffer = buffer;

  gtk_header_bar_set_show_close_button(GTK_HEADER_BAR(result), TRUE);
  gtk_header_bar_set_title(GTK_HEADER_BAR(result), "Collaborative Notepad");

  result->button_stack = gtk_stack_new();
  gtk_header_bar_pack_start(GTK_HEADER_BAR(result), result->button_stack);

  result->add_button = gtk_menu_button_new();
  gtk_container_add(GTK_CONTAINER(result->button_stack), result->add_button);
  gtk_widget_set_tooltip_text(result->add_button, "Add File");

  GtkWidget *add_icon =
      gtk_image_new_from_icon_name("list-add-symbolic", GTK_ICON_SIZE_MENU);
  gtk_button_set_image(GTK_BUTTON(result->add_button), add_icon);

  result->back_button = gtk_button_new();
  gtk_container_add(GTK_CONTAINER(result->button_stack), result->back_button);
  gtk_widget_set_tooltip_text(result->back_button, "Back");
  g_signal_connect_swapped(result->back_button, "clicked",
                           G_CALLBACK(close_file_cb), result);

  GtkWidget *back_icon =
      gtk_image_new_from_icon_name("go-previous-symbolic", GTK_ICON_SIZE_MENU);
  gtk_button_set_image(GTK_BUTTON(result->back_button), back_icon);

  result->add_popover = gtk_popover_new(result->add_button);
  gtk_menu_button_set_popover(GTK_MENU_BUTTON(result->add_button),
                              result->add_popover);
  g_signal_connect_swapped(result->add_button, "clicked",
                           G_CALLBACK(show_add_popover_cb), result);

  GtkWidget *box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 12);
  gtk_container_add(GTK_CONTAINER(result->add_popover), box);
  GValue margin = G_VALUE_INIT;
  g_value_init(&margin, G_TYPE_UINT);
  g_value_set_uint(&margin, 6);
  g_object_set_property(G_OBJECT(box), "margin", &margin);
  g_value_unset(&margin);

  result->entry = gtk_entry_new();
  gtk_container_add(GTK_CONTAINER(box), result->entry);
  g_signal_connect_swapped(result->entry, "activate", G_CALLBACK(add_file_cb),
                           result);

  GtkWidget *add_button = gtk_button_new_with_label("Add File");
  gtk_container_add(GTK_CONTAINER(box), add_button);
  GtkStyleContext *style_context = gtk_widget_get_style_context(add_button);
  gtk_style_context_add_class(style_context, "suggested-action");
  g_signal_connect_swapped(add_button, "clicked", G_CALLBACK(add_file_cb),
                           result);

  GtkWidget *menu_button = gtk_menu_button_new();
  gtk_header_bar_pack_end(GTK_HEADER_BAR(result), menu_button);
  GtkWidget *menu_icon =
      gtk_image_new_from_icon_name("open-menu-symbolic", GTK_ICON_SIZE_MENU);
  gtk_button_set_image(GTK_BUTTON(menu_button), menu_icon);

  result->menu_popover = gtk_popover_menu_new();
  gtk_menu_button_set_popover(GTK_MENU_BUTTON(menu_button),
                              result->menu_popover);
  g_signal_connect_swapped(menu_button, "clicked",
                           G_CALLBACK(show_menu_popover_cb), result);

  GtkWidget *menu_box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 8);
  gtk_container_add(GTK_CONTAINER(result->menu_popover), menu_box);
  GValue menu_box_margin = G_VALUE_INIT;
  g_value_init(&menu_box_margin, G_TYPE_UINT);
  g_value_set_uint(&menu_box_margin, 6);
  g_object_set_property(G_OBJECT(menu_box), "margin", &menu_box_margin);
  g_value_unset(&menu_box_margin);

  result->preferences = collaborative_notepad_preferences_new(shared);

  GtkWidget *preferences_button = gtk_model_button_new();
  gtk_container_add(GTK_CONTAINER(menu_box), preferences_button);
  GValue preferences_button_text = G_VALUE_INIT;
  g_value_init(&preferences_button_text, G_TYPE_STRING);
  g_value_set_string(&preferences_button_text, "Preferences");
  g_object_set_property(G_OBJECT(preferences_button), "text",
                        &preferences_button_text);
  g_value_unset(&preferences_button_text);
  g_signal_connect_swapped(preferences_button, "clicked",
                           G_CALLBACK(show_preferences_cb), result);

  GtkWidget *about_button = gtk_model_button_new();
  gtk_container_add(GTK_CONTAINER(menu_box), about_button);
  GValue about_button_text = G_VALUE_INIT;
  g_value_init(&about_button_text, G_TYPE_STRING);
  g_value_set_string(&about_button_text, "About");
  g_object_set_property(G_OBJECT(about_button), "text", &about_button_text);
  g_value_unset(&about_button_text);
  g_signal_connect_swapped(about_button, "clicked", G_CALLBACK(show_about_cb),
                           result);

  result->editor_actions_revealer = gtk_revealer_new();
  gtk_revealer_set_transition_type(
      GTK_REVEALER(result->editor_actions_revealer),
      GTK_REVEALER_TRANSITION_TYPE_SLIDE_RIGHT);
  gtk_header_bar_pack_end(GTK_HEADER_BAR(result),
                          result->editor_actions_revealer);

  GtkWidget *editor_actions = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 8);
  gtk_container_add(GTK_CONTAINER(result->editor_actions_revealer),
                    GTK_WIDGET(editor_actions));
  gtk_revealer_set_reveal_child(GTK_REVEALER(result->editor_actions_revealer),
                                FALSE);

  result->language_combo =
      collaborative_notepad_language_combo_new(language_store, shared);
  gtk_container_add(GTK_CONTAINER(editor_actions),
                    GTK_WIDGET(result->language_combo));

  GtkWidget *save_button = gtk_button_new();
  gtk_container_add(GTK_CONTAINER(editor_actions), save_button);
  gtk_widget_set_tooltip_text(save_button, "Save");
  GtkWidget *save_icon =
      gtk_image_new_from_icon_name("document-save-symbolic", GTK_ICON_SIZE_MENU);
  gtk_button_set_image(GTK_BUTTON(save_button), save_icon);
  g_signal_connect_swapped(save_button, "clicked",
                           G_CALLBACK(save_cb), result);

  return result;
}
