#ifndef REMOVE_BUTTON_H
#define REMOVE_BUTTON_H

#include <gtk/gtk.h>
#include <stdbool.h>
#include "../shared.h"

G_BEGIN_DECLS

#define COLLABORATIVE_NOTEPAD_REMOVE_BUTTON_TYPE                               \
  collaborative_notepad_remove_button_get_type()
G_DECLARE_FINAL_TYPE(CollaborativeNotepadRemoveButton,
                     collaborative_notepad_remove_button, COLLABORATIVE_NOTEPAD,
                     REMOVE_BUTTON, GtkCellRendererPixbuf)

CollaborativeNotepadRemoveButton *
collaborative_notepad_remove_button_new(GtkListStore *list_store, shared_data_t *shared);

G_END_DECLS

#endif // REMOVE_BUTTON_H
