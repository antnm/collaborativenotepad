#include "preferences.h"
#include "../../message.h"
#include <sys/socket.h>
#include <unistd.h>

struct _CollaborativeNotepadPreferences {
  GtkDialog parent_instance;
  shared_data_t *shared;
  GtkWidget *address_entry;
  GtkWidget *port_button;
};

G_DEFINE_TYPE(CollaborativeNotepadPreferences,
              collaborative_notepad_preferences, GTK_TYPE_DIALOG)

void collaborative_notepad_preferences_init(
    CollaborativeNotepadPreferences *preferences) {}

void collaborative_notepad_preferences_class_init(
    CollaborativeNotepadPreferencesClass *class) {}

void collaborative_notepad_preferences_show(
    CollaborativeNotepadPreferences *preferences) {
  pthread_mutex_lock(preferences->shared->mutex);
  gtk_entry_set_text(GTK_ENTRY(preferences->address_entry),
                     preferences->shared->address);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(preferences->port_button),
                            preferences->shared->port);
  gtk_widget_show_all(GTK_WIDGET(preferences));
  pthread_mutex_unlock(preferences->shared->mutex);
}

void apply_cb(CollaborativeNotepadPreferences *preferences) {
  const gchar *new_address =
      gtk_entry_get_text(GTK_ENTRY(preferences->address_entry));
  uint16_t new_port = gtk_spin_button_get_value_as_int(
      GTK_SPIN_BUTTON(preferences->port_button));
  reconnect(preferences->shared, new_address, new_port);

  gtk_widget_hide(GTK_WIDGET(preferences));
}

void cancel_cb(CollaborativeNotepadPreferences *preferences) {
  gtk_widget_hide(GTK_WIDGET(preferences));
}

CollaborativeNotepadPreferences *
collaborative_notepad_preferences_new(shared_data_t *shared) {
  CollaborativeNotepadPreferences *result =
      g_object_new(COLLABORATIVE_NOTEPAD_PREFERENCES_TYPE, NULL);
  result->shared = shared;
  gtk_window_set_resizable(GTK_WINDOW(result), FALSE);

  GtkWidget *content_area = gtk_dialog_get_content_area(GTK_DIALOG(result));

  GtkWidget *header_bar = gtk_header_bar_new();
  gtk_window_set_titlebar(GTK_WINDOW(result), header_bar);

  GtkWidget *apply_button = gtk_button_new_with_label("Apply");
  gtk_header_bar_pack_end(GTK_HEADER_BAR(header_bar), apply_button);
  GtkStyleContext *style_context = gtk_widget_get_style_context(apply_button);
  gtk_style_context_add_class(style_context, "suggested-action");
  g_signal_connect_swapped(apply_button, "clicked", G_CALLBACK(apply_cb),
                           result);

  GtkWidget *cancel_button = gtk_button_new_with_label("Cancel");
  gtk_header_bar_pack_start(GTK_HEADER_BAR(header_bar), cancel_button);
  g_signal_connect_swapped(cancel_button, "clicked", G_CALLBACK(cancel_cb),
                           result);

  GtkWidget *grid = gtk_grid_new();
  GValue margin = G_VALUE_INIT;
  g_value_init(&margin, G_TYPE_UINT);
  g_value_set_uint(&margin, 6);
  g_object_set_property(G_OBJECT(grid), "margin", &margin);
  g_value_unset(&margin);
  gtk_container_add(GTK_CONTAINER(content_area), grid);
  gtk_grid_set_column_spacing(GTK_GRID(grid), 12);
  gtk_grid_set_row_spacing(GTK_GRID(grid), 12);

  GtkWidget *address_label = gtk_label_new("Address:");
  gtk_grid_attach(GTK_GRID(grid), address_label, 0, 0, 1, 1);

  result->address_entry = gtk_entry_new();
  gtk_grid_attach_next_to(GTK_GRID(grid), result->address_entry, address_label,
                          GTK_POS_RIGHT, 1, 1);
  gtk_widget_set_valign(GTK_WIDGET(result->address_entry), GTK_ALIGN_END);

  GtkWidget *port_label = gtk_label_new("Port:");
  gtk_grid_attach(GTK_GRID(grid), port_label, 0, 1, 1, 1);

  result->port_button = gtk_spin_button_new_with_range(0, 65535, 1);
  gtk_grid_attach_next_to(GTK_GRID(grid), result->port_button, port_label,
                          GTK_POS_RIGHT, 1, 1);
  gtk_widget_set_valign(GTK_WIDGET(result->port_button), GTK_ALIGN_END);

  return result;
}
