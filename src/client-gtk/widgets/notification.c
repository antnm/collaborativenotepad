#include "notification.h"
#include "gtk/gtkrevealer.h"
#include <unistd.h>

struct _CollaborativeNotepadNotification {
  GtkRevealer parent_instance;
  GtkWidget *label;
};

G_DEFINE_TYPE(CollaborativeNotepadNotification,
              collaborative_notepad_notification, GTK_TYPE_REVEALER)

void collaborative_notepad_notification_init(
    CollaborativeNotepadNotification *notification) {}

void collaborative_notepad_notification_class_init(
    CollaborativeNotepadNotificationClass *class) {}

void close_cb(CollaborativeNotepadNotification *notification, gpointer data) {
  gtk_revealer_set_reveal_child(GTK_REVEALER(notification), FALSE);
}

void collaborative_notepad_notification_show(CollaborativeNotepadNotification *notification, char *message) {
  gtk_label_set_label(GTK_LABEL(notification->label), message);
  gtk_revealer_set_reveal_child(GTK_REVEALER(notification), TRUE);
}

CollaborativeNotepadNotification *
collaborative_notepad_notification_new(int *sd) {
  CollaborativeNotepadNotification *result =
      g_object_new(COLLABORATIVE_NOTEPAD_NOTIFICATION_TYPE, NULL);

  gtk_widget_set_halign(GTK_WIDGET(result), GTK_ALIGN_CENTER);
  gtk_widget_set_valign(GTK_WIDGET(result), GTK_ALIGN_START);

  GtkWidget *box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
  gtk_container_add(GTK_CONTAINER(result), box);

  GtkStyleContext *style_context = gtk_widget_get_style_context(box);
  gtk_style_context_add_class(style_context, "app-notification");

  result->label = gtk_label_new("");
  gtk_container_add(GTK_CONTAINER(box), result->label);
  gtk_label_set_line_wrap(GTK_LABEL(result->label), TRUE);

  GtkWidget *close_button = gtk_button_new_from_icon_name(
      "window-close-symbolic", GTK_ICON_SIZE_MENU);
  gtk_container_add(GTK_CONTAINER(box), close_button);
  gtk_button_set_relief(GTK_BUTTON(close_button), GTK_RELIEF_NONE);
  g_signal_connect_swapped(close_button, "clicked", G_CALLBACK(close_cb),
                           result);

  return result;
}
