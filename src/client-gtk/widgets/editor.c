#include "editor.h"
#include "../../message.h"
#include "../window.h"
#include "../shared.h"
#include <string.h>

struct _CollaborativeNotepadEditor {
  GtkScrolledWindow parent_instance;
  GtkWidget *view;
  CollaborativeNotepadEditorBuffer *buffer;
};

G_DEFINE_TYPE(CollaborativeNotepadEditor, collaborative_notepad_editor,
              GTK_TYPE_SCROLLED_WINDOW)

void collaborative_notepad_editor_init(CollaborativeNotepadEditor *editor) {}

void collaborative_notepad_editor_class_init(
    CollaborativeNotepadEditorClass *class) {}

void collaborative_notepad_editor_scroll_to_cursor(
    CollaborativeNotepadEditor *editor) {
  gtk_text_view_scroll_mark_onscreen(
      GTK_TEXT_VIEW(editor->view),
      gtk_text_buffer_get_insert(GTK_TEXT_BUFFER(editor->buffer)));
}

CollaborativeNotepadEditor *collaborative_notepad_editor_new(
    CollaborativeNotepadEditorBuffer *editor_buffer, shared_data_t *shared) {
  CollaborativeNotepadEditor *result =
      g_object_new(COLLABORATIVE_NOTEPAD_EDITOR_TYPE, NULL);
  result->buffer = editor_buffer;
  result->view =
      gtk_source_view_new_with_buffer(GTK_SOURCE_BUFFER(editor_buffer));
  gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(result->view), GTK_WRAP_CHAR);
  gtk_text_view_set_monospace(GTK_TEXT_VIEW(result->view), true);
  gtk_source_view_set_highlight_current_line(GTK_SOURCE_VIEW(result->view),
                                             true);
  gtk_source_view_set_show_line_numbers(GTK_SOURCE_VIEW(result->view), true);
  gtk_container_add(GTK_CONTAINER(result), result->view);

  GtkSourceMarkAttributes *attrs = gtk_source_mark_attributes_new();
  GdkRGBA rgba;
  gdk_rgba_parse(&rgba, "rgb(237,51,59)");
  gtk_source_mark_attributes_set_background(attrs, &rgba);
  gtk_source_view_set_mark_attributes(GTK_SOURCE_VIEW(result->view), "other",
                                      attrs, 0);

  return result;
}
