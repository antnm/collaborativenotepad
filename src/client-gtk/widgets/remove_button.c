#include "remove_button.h"
#include "../../message.h"
#include "../file_list_store.h"

struct _CollaborativeNotepadRemoveButton {
  GtkCellRendererPixbuf parent_instance;
  GtkListStore *list_store;
  shared_data_t *shared;
};

G_DEFINE_TYPE(CollaborativeNotepadRemoveButton,
              collaborative_notepad_remove_button,
              GTK_TYPE_CELL_RENDERER_PIXBUF)

void collaborative_notepad_remove_button_init(
    CollaborativeNotepadRemoveButton *remove_button) {}

gboolean remove_button_activate(GtkCellRenderer *cell, GdkEvent *event,
                                GtkWidget *widget, const gchar *path,
                                const GdkRectangle *background_area,
                                const GdkRectangle *cell_area,
                                GtkCellRendererState flags) {
  if (!connected(COLLABORATIVE_NOTEPAD_REMOVE_BUTTON(cell)->shared))
    return TRUE;

  GtkTreeIter iter;
  gtk_tree_model_get_iter_first(
      GTK_TREE_MODEL(COLLABORATIVE_NOTEPAD_REMOVE_BUTTON(cell)->list_store),
      &iter);
  int position = atoi(path);
  for (int i = 0; i < position; ++i)
    gtk_tree_model_iter_next(
        GTK_TREE_MODEL(COLLABORATIVE_NOTEPAD_REMOVE_BUTTON(cell)->list_store),
        &iter);

  GValue file_name = G_VALUE_INIT;
  gtk_tree_model_get_value(
      GTK_TREE_MODEL(COLLABORATIVE_NOTEPAD_REMOVE_BUTTON(cell)->list_store),
      &iter, FILE_NAME, &file_name);

  request req;
  req.type = REQUEST_REMOVE_FILE;
  req.str.size = strlen(g_value_get_string(&file_name)) + 1;
  req.str.data = malloc(req.str.size);
  strcpy(req.str.data, g_value_get_string(&file_name));

  if (!send_request(req, COLLABORATIVE_NOTEPAD_REMOVE_BUTTON(cell)->shared->sd))
    disconnect(COLLABORATIVE_NOTEPAD_REMOVE_BUTTON(cell)->shared);

  free_request(req);
  g_value_unset(&file_name);

  return TRUE;
}

void collaborative_notepad_remove_button_class_init(
    CollaborativeNotepadRemoveButtonClass *class) {
  g_signal_new("activate", COLLABORATIVE_NOTEPAD_REMOVE_BUTTON_TYPE,
               G_SIGNAL_RUN_LAST, 0, NULL, NULL, NULL, G_TYPE_BOOLEAN, 7,
               GTK_TYPE_CELL_RENDERER_PIXBUF, GDK_TYPE_EVENT, GTK_TYPE_WIDGET,
               G_TYPE_STRING, GDK_TYPE_RECTANGLE, GDK_TYPE_RECTANGLE,
               GTK_TYPE_CELL_RENDERER_STATE);
  class->parent_class.parent_class.activate = remove_button_activate;
}

CollaborativeNotepadRemoveButton *
collaborative_notepad_remove_button_new(GtkListStore *list_store, shared_data_t *shared) {
  CollaborativeNotepadRemoveButton *result =
      g_object_new(COLLABORATIVE_NOTEPAD_REMOVE_BUTTON_TYPE, NULL);
  result->list_store = list_store;
  result->shared = shared;

  GValue remove_icon_name = G_VALUE_INIT;
  g_value_init(&remove_icon_name, G_TYPE_STRING);
  g_value_set_string(&remove_icon_name, "edit-delete-symbolic");
  g_object_set_property(G_OBJECT(result), "icon-name", &remove_icon_name);
  g_value_unset(&remove_icon_name);
  GValue activatable_mode = G_VALUE_INIT;
  g_value_init(&activatable_mode, GTK_TYPE_CELL_RENDERER_MODE);
  g_value_set_enum(&activatable_mode, GTK_CELL_RENDERER_MODE_ACTIVATABLE);
  g_object_set_property(G_OBJECT(result), "mode", &activatable_mode);
  g_value_unset(&activatable_mode);
  g_signal_connect(result, "activate", G_CALLBACK(result), NULL);

  return result;
}
