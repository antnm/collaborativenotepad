#ifndef EDITOR_H
#define EDITOR_H

#include "../editor_buffer.h"
#include "../shared.h"
#include <gtk/gtk.h>
#include <gtksourceview/gtksource.h>
#include <stdbool.h>

G_BEGIN_DECLS

#define COLLABORATIVE_NOTEPAD_EDITOR_TYPE                                      \
  collaborative_notepad_editor_get_type()
G_DECLARE_FINAL_TYPE(CollaborativeNotepadEditor, collaborative_notepad_editor,
                     COLLABORATIVE_NOTEPAD, EDITOR, GtkSourceView)

void collaborative_notepad_editor_scroll_to_cursor(
    CollaborativeNotepadEditor *editor);
CollaborativeNotepadEditor *collaborative_notepad_editor_new(
    CollaborativeNotepadEditorBuffer *editor_buffer, shared_data_t *shared);

G_END_DECLS

#endif // EDITOR_H
