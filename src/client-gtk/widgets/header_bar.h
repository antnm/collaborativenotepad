#ifndef HEADER_BAR_H
#define HEADER_BAR_H

#include "../responses.h"
#include <gtk/gtk.h>
#include <stdbool.h>

G_BEGIN_DECLS

#define COLLABORATIVE_NOTEPAD_HEADER_BAR_TYPE                                  \
  collaborative_notepad_header_bar_get_type()
G_DECLARE_FINAL_TYPE(CollaborativeNotepadHeaderBar,
                     collaborative_notepad_header_bar, COLLABORATIVE_NOTEPAD,
                     HEADER_BAR, GtkHeaderBar)

void collaborative_notepad_header_bar_show_file_list(
    CollaborativeNotepadHeaderBar *header_bar);
void collaborative_notepad_header_bar_show_editor(
    CollaborativeNotepadHeaderBar *header_bar);
void collaborative_notepad_header_bar_select_language(
    CollaborativeNotepadHeaderBar *header_bar, char *language);
CollaborativeNotepadHeaderBar *
collaborative_notepad_header_bar_new(GtkListStore *language_store,
                                     shared_data_t *shared,
                                     CollaborativeNotepadEditorBuffer *buffer);

G_END_DECLS

#endif // HEADER_BAR_H
