#include "language_combo.h"
#include <stdlib.h>

struct _CollaborativeNotepadLanguageCombo {
  GtkComboBox parent_instance;
  shared_data_t *shared;
  bool manual_change;
};

G_DEFINE_TYPE(CollaborativeNotepadLanguageCombo,
              collaborative_notepad_language_combo, GTK_TYPE_COMBO_BOX)

void collaborative_notepad_language_combo_init(
    CollaborativeNotepadLanguageCombo *language_combo) {}

void collaborative_notepad_language_combo_class_init(
    CollaborativeNotepadLanguageComboClass *class) {}

void changed_override(CollaborativeNotepadLanguageCombo *language_combo,
                      gpointer user_data) {
  if (language_combo->manual_change) {
    g_signal_chain_from_overridden_handler(language_combo, user_data);
  } else {
    if (!connected(language_combo->shared))
      return;

    request req;
    req.type = REQUEST_CHANGE_FILE_LANGUAGE;
    const gchar *language_id =
      gtk_combo_box_get_active_id(GTK_COMBO_BOX(language_combo));
    req.str.size = strlen(language_id) + 1;
    req.str.data = malloc(req.str.size);
    strcpy(req.str.data, language_id);

    if (!send_request(req, language_combo->shared->sd))
      disconnect(language_combo->shared);

    free_request(req);
  }
}

void collaborative_notepad_language_combo_select_language(
    CollaborativeNotepadLanguageCombo *language_combo, char *language) {
  language_combo->manual_change = true;
  gtk_combo_box_set_active_id(GTK_COMBO_BOX(language_combo), language);
  g_signal_emit_by_name(language_combo, "changed", NULL);
  language_combo->manual_change = false;
}

CollaborativeNotepadLanguageCombo *
collaborative_notepad_language_combo_new(GtkListStore *language_store,
                                         shared_data_t *shared) {
  CollaborativeNotepadLanguageCombo *result =
      g_object_new(COLLABORATIVE_NOTEPAD_LANGUAGE_COMBO_TYPE, NULL);
  result->shared = shared;
  result->manual_change = false;
  gtk_combo_box_set_model(GTK_COMBO_BOX(result),
                          GTK_TREE_MODEL(language_store));
  gtk_combo_box_set_id_column(GTK_COMBO_BOX(result), 0);
  GtkCellRenderer *cell_name = gtk_cell_renderer_text_new();
  gtk_cell_layout_pack_start(GTK_CELL_LAYOUT(result), cell_name, TRUE);
  gtk_cell_layout_add_attribute(GTK_CELL_LAYOUT(result), cell_name, "text", 1);

  g_signal_override_class_handler("changed",
                                  COLLABORATIVE_NOTEPAD_LANGUAGE_COMBO_TYPE,
                                  G_CALLBACK(changed_override));

  return result;
}
