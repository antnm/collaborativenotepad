#ifndef FILE_LIST_H
#define FILE_LIST_H

#include <gtk/gtk.h>
#include <stdbool.h>
#include "../shared.h"

G_BEGIN_DECLS

#define COLLABORATIVE_NOTEPAD_FILE_LIST_TYPE                                   \
  collaborative_notepad_file_list_get_type()
G_DECLARE_FINAL_TYPE(CollaborativeNotepadFileList,
                     collaborative_notepad_file_list, COLLABORATIVE_NOTEPAD,
                     FILE_LIST, GtkScrolledWindow)

CollaborativeNotepadFileList *
collaborative_notepad_file_list_new(GtkListStore *list_store, shared_data_t *shared);

G_END_DECLS

#endif // FILE_LIST_H
