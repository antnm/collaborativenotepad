#ifndef FILE_LIST_STORE_H
#define FILE_LIST_STORE_H

#include <gtk/gtk.h>
#include <stdbool.h>

typedef enum {
  FILE_NAME,
  USER_COUNT,
  USER_COUNT_MARKUP
} file_list_store_columns;

void add_file(GtkListStore *list_store, char *file_name);
void remove_file(GtkListStore *list_store, char *file_name);
void open_file(GtkListStore *list_store, char *file_name);
bool is_file_open(GtkListStore *list_store, char *file_name);
void close_file(GtkListStore *list_store, char *file_name);

#endif // FILE_LIST_STORE_H
