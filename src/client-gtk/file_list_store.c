#include "file_list_store.h"

void add_file(GtkListStore *list_store, char *file_name) {
  gtk_list_store_insert_with_values(list_store, NULL, -1, FILE_NAME, file_name,
                                    USER_COUNT, 0, USER_COUNT_MARKUP, "", -1);
}

bool is_file_open(GtkListStore *list_store, char *file_name) {
  GtkTreeIter iter;
  gtk_tree_model_get_iter_first(GTK_TREE_MODEL(list_store), &iter);
  do {
    GValue file_name_iter = G_VALUE_INIT;
    gtk_tree_model_get_value(GTK_TREE_MODEL(list_store), &iter, FILE_NAME,
                             &file_name_iter);
    if (strcmp(file_name, g_value_get_string(&file_name_iter)) == 0) {
      GValue user_count = G_VALUE_INIT;
      gtk_tree_model_get_value(GTK_TREE_MODEL(list_store), &iter, USER_COUNT,
                               &user_count);
      if (g_value_get_uint(&user_count)) {
        g_value_unset(&user_count);
        g_value_unset(&file_name_iter);
        return true;
      }
      g_value_unset(&user_count);
    }
    g_value_unset(&file_name_iter);
  } while (gtk_tree_model_iter_next(GTK_TREE_MODEL(list_store), &iter));

  return false;
}

void remove_file(GtkListStore *list_store, char *file_name) {
  GtkTreeIter iter;
  gtk_tree_model_get_iter_first(GTK_TREE_MODEL(list_store), &iter);
  do {
    GValue file_name_iter = G_VALUE_INIT;
    gtk_tree_model_get_value(GTK_TREE_MODEL(list_store), &iter, FILE_NAME,
                             &file_name_iter);
    if (strcmp(file_name, g_value_get_string(&file_name_iter)) == 0) {
      gtk_list_store_remove(list_store, &iter);
      g_value_unset(&file_name_iter);
      break;
    }
    g_value_unset(&file_name_iter);
  } while (gtk_tree_model_iter_next(GTK_TREE_MODEL(list_store), &iter));
}

void open_file(GtkListStore *list_store, char *file_name) {
  GtkTreeIter iter;
  gtk_tree_model_get_iter_first(GTK_TREE_MODEL(list_store), &iter);
  do {
    GValue file_name_iter = G_VALUE_INIT;
    gtk_tree_model_get_value(GTK_TREE_MODEL(list_store), &iter, FILE_NAME,
                             &file_name_iter);
    if (strcmp(file_name, g_value_get_string(&file_name_iter)) == 0) {
      GValue user_count = G_VALUE_INIT;
      gtk_tree_model_get_value(GTK_TREE_MODEL(list_store), &iter, USER_COUNT,
                               &user_count);
      g_value_set_uint(&user_count, g_value_get_uint(&user_count) + 1);

      gtk_list_store_set_value(list_store, &iter, USER_COUNT, &user_count);

      GValue user_count_markup = G_VALUE_INIT;
      g_value_init(&user_count_markup, G_TYPE_STRING);

      char *str =
          g_markup_printf_escaped("<i>%d/2</i>", g_value_get_uint(&user_count));
      g_value_set_string(&user_count_markup, str);
      g_free(str);

      gtk_list_store_set_value(list_store, &iter, USER_COUNT_MARKUP,
                               &user_count_markup);

      g_value_unset(&user_count);
      g_value_unset(&user_count_markup);
      g_value_unset(&file_name_iter);
      break;
    }
    g_value_unset(&file_name_iter);
  } while (gtk_tree_model_iter_next(GTK_TREE_MODEL(list_store), &iter));
}

void close_file(GtkListStore *list_store, char *file_name) {
  GtkTreeIter iter;
  gtk_tree_model_get_iter_first(GTK_TREE_MODEL(list_store), &iter);
  do {
    GValue file_name_iter = G_VALUE_INIT;
    gtk_tree_model_get_value(GTK_TREE_MODEL(list_store), &iter, FILE_NAME,
                             &file_name_iter);
    if (strcmp(file_name, g_value_get_string(&file_name_iter)) == 0) {
      GValue user_count = G_VALUE_INIT;
      gtk_tree_model_get_value(GTK_TREE_MODEL(list_store), &iter, USER_COUNT,
                               &user_count);
      g_value_set_uint(&user_count, g_value_get_uint(&user_count) - 1);
      gtk_list_store_set_value(list_store, &iter, USER_COUNT, &user_count);

      GValue user_count_markup = G_VALUE_INIT;
      g_value_init(&user_count_markup, G_TYPE_STRING);

      if (g_value_get_uint(&user_count) == 0) {
        g_value_set_static_string(&user_count_markup, "");
      } else {
        char *str = g_markup_printf_escaped("<i>%d/2</i>",
                                            g_value_get_uint(&user_count));
        g_value_set_string(&user_count_markup, str);
        g_free(str);
      }

      gtk_list_store_set_value(list_store, &iter, USER_COUNT_MARKUP,
                               &user_count_markup);

      g_value_unset(&user_count);
      g_value_unset(&user_count_markup);
      g_value_unset(&file_name_iter);
      break;
    }
    g_value_unset(&file_name_iter);
  } while (gtk_tree_model_iter_next(GTK_TREE_MODEL(list_store), &iter));
}
