#include "responses.h"
#include "../message.h"
#include "editor_buffer.h"
#include "file_list_store.h"
#include "shared.h"
#include "widgets/editor.h"
#include "window.h"
#include <arpa/inet.h>
#include <pthread.h>
#include <sys/socket.h>
#include <unistd.h>

gboolean process_responses(gpointer arg) {
  process_responses_data_t *data = arg;

  pthread_mutex_lock(data->shared->mutex);
  if (data->shared->should_clear) {
    data->shared->should_clear = false;
    gtk_list_store_clear(data->list_store);
    collaborative_notepad_window_show_file_list(data->window);
    collaborative_notepad_editor_buffer_clear(data->editor_buffer);
  }
  if (!data->shared->connected) {
    response rsp;
    while (read(data->pipefd, &rsp, sizeof(response)) == sizeof(response))
      free_response(rsp);
    pthread_mutex_unlock(data->shared->mutex);
    return TRUE;
  }
  uint32_t id = data->shared->id;
  pthread_mutex_unlock(data->shared->mutex);

  response rsp;
  while (read(data->pipefd, &rsp, sizeof(response)) == sizeof(response)) {
    switch (rsp.type) {
    case UPDATE_ADDED_FILE:
      add_file(data->list_store, rsp.str.data);
      break;

    case UPDATE_REMOVED_FILE:
      remove_file(data->list_store, rsp.str.data);
      break;

    case UPDATE_OPENED_FILE:
      open_file(data->list_store, rsp.str.data);
      if (rsp.id == id) {
        collaborative_notepad_window_show_editor(data->window);
        data->opened_file = realloc(data->opened_file, rsp.str.size);
        strcpy(data->opened_file, rsp.str.data);
      }
      break;

    case UPDATE_CLOSED_FILE:
      close_file(data->list_store, rsp.str.data);
      if (rsp.id == id) {
        collaborative_notepad_window_show_file_list(data->window);
        collaborative_notepad_editor_buffer_clear(data->editor_buffer);
        free(data->opened_file);
        data->opened_file = NULL;
      } else {
        collaborative_notepad_editor_buffer_remove_other(data->editor_buffer);
      }
      break;

    case UPDATE_CHANGED_FILE_LANGUAGE:
      collaborative_notepad_editor_buffer_set_language(data->editor_buffer,
                                                       rsp.str.data);
      collaborative_notepad_window_select_language(data->window,
                                                   rsp.str.data);
      break;

    case UPDATE_INSERT:
      if (rsp.str.size > 1)
        collaborative_notepad_editor_buffer_insert(
            data->editor_buffer, rsp.id == id, rsp.pos, rsp.str);
      break;

    case UPDATE_DELETE_RANGE:
      collaborative_notepad_editor_buffer_delete_range(
          data->editor_buffer, rsp.id == id, rsp.pos, rsp.pos_end);
      break;

    case UPDATE_CHANGE_POSITION:
      if (rsp.id == id)
        collaborative_notepad_window_scroll_to_cursor(data->window);
      else
        collaborative_notepad_editor_buffer_change_position_other(
            data->editor_buffer, rsp.pos, rsp.pos_end);
      break;

    case ERROR_OPENED_FILE_DOES_NOT_EXIST:
      collaborative_notepad_window_show_notification(
          data->window, "Opened file does not exist");
      break;

    case ERROR_OPENED_FILE_HAS_TOO_MANY_USERS:
      collaborative_notepad_window_show_notification(
          data->window, "Opened file has too many users");
      break;

    case ERROR_NO_FILE_TO_CLOSE:
      collaborative_notepad_window_show_notification(data->window,
                                                     "No file to close");
      break;

    case ERROR_ALREADY_ACCESSING_A_FILE:
      collaborative_notepad_window_show_notification(
          data->window, "Already accessing a file");
      break;

    case ERROR_CREATED_FILE_ALREADY_EXISTS:
      collaborative_notepad_window_show_notification(
          data->window, "Created file already exists");
      break;

    case ERROR_REMOVED_FILE_DOES_NOT_EXIST:
      collaborative_notepad_window_show_notification(
          data->window, "Removed file does not exist");
      break;

    case ERROR_REMOVED_FILE_IS_IN_USE:
      collaborative_notepad_window_show_notification(data->window,
                                                     "Removed file is in use");
      break;

    case ERROR_NO_FILE_TO_EDIT:
      collaborative_notepad_window_show_notification(data->window,
                                                     "No file open to edit");
      break;
    }
    free_response(rsp);
  }

  return TRUE;
}

void *recieve_responses(void *arg) {
  recieve_responses_data_t *data = arg;
  for (;;) {
    pthread_mutex_lock(data->shared->mutex);
    if (data->shared->quit) {
      pthread_mutex_unlock(data->shared->mutex);
      pthread_exit(NULL);
    }

    if (!data->shared->connected) {
      if ((data->shared->sd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
        perror("Could not create socket");

      struct sockaddr_in addr = {.sin_family = AF_INET,
                                 .sin_addr.s_addr =
                                     inet_addr(data->shared->address),
                                 .sin_port = htons(data->shared->port)};
      if (connect(data->shared->sd, (struct sockaddr *)&addr,
                  sizeof(struct sockaddr)) == -1) {
        perror("Could not connect");
        pthread_mutex_unlock(data->shared->mutex);
        sleep(1);
        continue;
      }

      if (!recieve_id(&data->shared->id, data->shared->sd)) {
        close(data->shared->sd);
        pthread_mutex_unlock(data->shared->mutex);
        sleep(1);
        continue;
      }
      data->shared->connected = true;
    }
    pthread_mutex_unlock(data->shared->mutex);

    response rsp;
    if (!recieve_response(&rsp, data->shared->sd)) {
      disconnect(data->shared);
      continue;
    }

    write(data->pipefd, &rsp, sizeof(response));
  }
}
