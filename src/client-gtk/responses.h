#ifndef RESPONSES_H
#define RESPONSES_H

#include <gtk/gtk.h>
#include <pthread.h>
#include <stdbool.h>
#include "window.h"
#include "shared.h"

typedef struct {
  shared_data_t *shared;
  GtkListStore *list_store;
  GtkListStore *language_store;
  CollaborativeNotepadEditorBuffer *editor_buffer;
  CollaborativeNotepadWindow *window;
  char *opened_file;
  int pipefd;
} process_responses_data_t;

gboolean process_responses(gpointer arg);

typedef struct {
  shared_data_t *shared;
  int pipefd;
} recieve_responses_data_t;

void *recieve_responses(void *arg);

#endif // RESPONSES_H
