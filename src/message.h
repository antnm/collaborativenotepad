#ifndef MESSAGE_H
#define MESSAGE_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

typedef struct {
  uint32_t size;
  char *data;
} string;

typedef struct {
  int32_t line;
  int32_t column;
} position;

bool send_id(uint32_t id, int desc);
bool recieve_id(uint32_t *id, int desc);

typedef enum request_type {
  REQUEST_CREATE_FILE,          // str
  REQUEST_REMOVE_FILE,          // str
  REQUEST_OPEN_FILE,            // str
  REQUEST_CHANGE_FILE_LANGUAGE, // str
  REQUEST_INSERT,               // pos, str
  REQUEST_DELETE_RANGE,         // pos, pos_end
  REQUEST_CHANGE_POSITION,      // pos, pos_end
  REQUEST_CLOSE_FILE            // nothing
} request_type;

typedef struct request {
  uint8_t type; // request_type
  position pos;
  union {
    position pos_end;
    string str;
  };
} request;

bool send_request(request req, int desc);
bool recieve_request(request *req, int desc);
void free_request(request req);

typedef enum response_type {
  ERROR_OPENED_FILE_DOES_NOT_EXIST,
  ERROR_OPENED_FILE_HAS_TOO_MANY_USERS,
  ERROR_NO_FILE_TO_CLOSE,
  ERROR_ALREADY_ACCESSING_A_FILE,
  ERROR_CREATED_FILE_ALREADY_EXISTS,
  ERROR_REMOVED_FILE_DOES_NOT_EXIST,
  ERROR_REMOVED_FILE_IS_IN_USE,
  ERROR_NO_FILE_TO_EDIT,

  UPDATE_ADDED_FILE,            // str
  UPDATE_REMOVED_FILE,          // str
  UPDATE_CHANGED_FILE_LANGUAGE, // str
  UPDATE_CLOSED_FILE,           // id, str
  UPDATE_OPENED_FILE,           // id, str
  UPDATE_INSERT,                // id, pos, str
  UPDATE_DELETE_RANGE,          // id, pos, pos_end
  UPDATE_CHANGE_POSITION,       // id, pos, pos_end
} response_type;

typedef struct response {
  uint8_t type; // response_type
  uint32_t id;
  position pos;
  union {
    position pos_end;
    string str;
  };
} response;

bool send_response(response rsp, int desc);
bool recieve_response(response *rsp, int desc);
void free_response(response rsp);

#endif // MESSAGE_H
