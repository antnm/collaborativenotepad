#include "database.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

sqlite3 *new_db() {
  sqlite3 *db;

  if (sqlite3_open("database.sqlite3", &db)) {
    fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(db));
    sqlite3_close(db);
  }

  char *error_message;
  if (sqlite3_exec(db,
                   "CREATE TABLE IF NOT EXISTS files ( "
                   "  name TEXT PRIMARY KEY, "
                   "  content TEXT, "
                   "  language TEXT "
                   ");",
                   NULL, NULL, &error_message) != SQLITE_OK) {
    fprintf(stderr, "SQL error: %s\n", error_message);
    sqlite3_free(error_message);
  }

  return db;
}

bool file_exists_in_db(sqlite3 *db, char *file_name) {
  sqlite3_stmt *stmt;

  if (sqlite3_prepare_v2(db,
                         "SELECT COUNT(*) "
                         "FROM files "
                         "WHERE name = ?;",
                         -1, &stmt, NULL) != SQLITE_OK) {
    fprintf(stderr, "SQL error: %s\n", sqlite3_errmsg(db));
  }

  sqlite3_bind_text(stmt, 1, file_name, -1, NULL);

  sqlite3_step(stmt);
  bool result = sqlite3_column_int(stmt, 0) > 0;
  sqlite3_finalize(stmt);

  return result;
}

void create_file_in_db(sqlite3 *db, char *file_name) {
  sqlite3_stmt *stmt;

  if (sqlite3_prepare_v2(db,
                         "INSERT INTO files (name, content, language) "
                         "VALUES (?, '', '');",
                         -1, &stmt, NULL) != SQLITE_OK) {
    fprintf(stderr, "SQL error: %s\n", sqlite3_errmsg(db));
  }

  if (sqlite3_bind_text(stmt, 1, file_name, -1, NULL) != SQLITE_OK) {
    fprintf(stderr, "SQL error: %s\n", sqlite3_errmsg(db));
  }

  sqlite3_step(stmt);
  sqlite3_finalize(stmt);
}

void remove_file_from_db(sqlite3 *db, char *file_name) {
  sqlite3_stmt *stmt;

  if (sqlite3_prepare_v2(db,
                         "DELETE FROM files "
                         "WHERE name = ?;",
                         -1, &stmt, NULL) != SQLITE_OK) {
    fprintf(stderr, "SQL error: %s\n", sqlite3_errmsg(db));
  }

  if (sqlite3_bind_text(stmt, 1, file_name, -1, NULL) != SQLITE_OK) {
    fprintf(stderr, "SQL error: %s\n", sqlite3_errmsg(db));
  }

  sqlite3_step(stmt);
  sqlite3_finalize(stmt);
}

void get_file_list(sqlite3 *db, string **file_list, size_t *file_list_size) {
  sqlite3_stmt *stmt;
  if (sqlite3_prepare_v2(db, "SELECT name FROM files;", -1, &stmt, NULL) !=
      SQLITE_OK) {
    fprintf(stderr, "SQL error: %s\n", sqlite3_errmsg(db));
  }

  *file_list = NULL;
  *file_list_size = 0;

  while (sqlite3_step(stmt) != SQLITE_DONE) {
    *file_list = realloc(*file_list, (*file_list_size + 1) * sizeof(string));

    (*file_list)[*file_list_size].size = sqlite3_column_bytes(stmt, 0) + 1;

    (*file_list)[*file_list_size].data =
        malloc((*file_list)[*file_list_size].size);

    strcpy((*file_list)[*file_list_size].data,
           (const char *)(sqlite3_column_text(stmt, 0)));

    ++(*file_list_size);
  }

  sqlite3_finalize(stmt);
}

void update_file(sqlite3 *db, char *file_name, char *content, char *language) {
  sqlite3_stmt *stmt;

  if (sqlite3_prepare_v2(db,
                         "UPDATE files "
                         "SET content = ?, "
                         "    language = ? "
                         "WHERE name = ?;",
                         -1, &stmt, NULL) != SQLITE_OK) {
    fprintf(stderr, "SQL error: %s\n", sqlite3_errmsg(db));
  }

  sqlite3_bind_text(stmt, 1, content, -1, NULL);
  sqlite3_bind_text(stmt, 2, language, -1, NULL);
  sqlite3_bind_text(stmt, 3, file_name, -1, NULL);

  sqlite3_step(stmt);
  sqlite3_finalize(stmt);
}

void get_file(sqlite3 *db, char *file_name, char **content, char **language) {
  sqlite3_stmt *stmt;
  if (sqlite3_prepare_v2(db,
                         "SELECT content, language "
                         "FROM files "
                         "WHERE name = ?;",
                         -1, &stmt, NULL) != SQLITE_OK) {
    fprintf(stderr, "SQL error: %s\n", sqlite3_errmsg(db));
  }

  sqlite3_bind_text(stmt, 1, file_name, -1, NULL);

  sqlite3_step(stmt);

  *content = malloc(sqlite3_column_bytes(stmt, 0) + 1);
  if (sqlite3_column_text(stmt, 0) == NULL)
    (*content)[0] = '\0';
  else
    strcpy(*content, (const char *)sqlite3_column_text(stmt, 0));

  if (sqlite3_column_text(stmt, 1) == NULL) {
    *language = NULL;
  } else {
    *language = malloc(sqlite3_column_bytes(stmt, 1) + 1);
    strcpy(*language, (const char *)sqlite3_column_text(stmt, 1));
  }

  sqlite3_finalize(stmt);
}
