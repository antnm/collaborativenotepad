#ifndef THREAD_H
#define THREAD_H

#include "../message.h"
#include <pthread.h>
#include <sqlite3.h>
#include <stdbool.h>
#include <stddef.h>

struct shared_thread_data;
struct thread_data;

typedef struct {
  struct thread_data *accessors[2];
  position pos[2];
  position pos_end[2];
  char *file;
  char *content;
  char *language;
  pthread_mutex_t mutex;
} file_data;

typedef struct thread_data {
  uint32_t id;
  int client_desc;
  file_data *file;
  pthread_mutex_t mutex;
  struct shared_thread_data *shared;
} thread_data;

typedef struct thread_data_list {
  thread_data *node;
  struct thread_data_list *next;
} thread_data_list;

typedef struct file_data_list {
  file_data *node;
  struct file_data_list *next;
} file_data_list;

typedef struct shared_thread_data {
  sqlite3 *db;
  thread_data_list *list;
  pthread_mutex_t mutex;
  file_data_list *file_list;
} shared_thread_data;

bool add_thread(thread_data *thread);
void remove_thread(thread_data *thread);
bool open_file(thread_data *thread, string file_name);
bool close_file(thread_data *thread);
bool create_file(thread_data *thread, string file_name);
bool remove_file(thread_data *thread, string file_name);
bool change_file_language(thread_data *thread, string language);
bool insert(thread_data *thread, position pos, string str);
bool change_position(thread_data *thread, position pos, position pos_end);
bool delete_range(thread_data *thread, position pos, position pos_end);

#endif // THREAD_H
