#ifndef DATABASE_H
#define DATABASE_H

#include <sqlite3.h>
#include <stdbool.h>
#include "../message.h"

sqlite3 *new_db();
bool file_exists_in_db(sqlite3 *db, char *file_name);
void create_file_in_db(sqlite3 *db, char *file_name);
void remove_file_from_db(sqlite3 *db, char *file_name);
void get_file_list(sqlite3 *db, string **file_list, size_t *file_list_size);
void get_file(sqlite3 *db, char *file_name, char **content, char **language);
void update_file(sqlite3 *db, char *file_name, char *content, char *language);

#endif // DATABASE_H
