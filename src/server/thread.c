#include "thread.h"
#include "database.h"
#include "src/message.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>

bool send_response_to_everyone(response rsp, thread_data *thread) {
  bool result = true;

  for (thread_data_list *i = thread->shared->list; i != NULL; i = i->next) {
    pthread_mutex_lock(&i->node->mutex);

    if (!send_response(rsp, i->node->client_desc)) {
      if (i->node->id == thread->id)
        result = false;
      else
        shutdown(i->node->client_desc, SHUT_RDWR);
    }

    pthread_mutex_unlock(&i->node->mutex);
  }

  return result;
}

bool send_response_to_clients_accessing_same_file(response rsp,
                                                  thread_data *thread) {
  bool result = true;

  for (size_t i = 0; i < 2; ++i) {
    if (thread->file->accessors[i] == NULL)
      continue;
    pthread_mutex_lock(&thread->file->accessors[i]->mutex);

    if (!send_response(rsp, thread->file->accessors[i]->client_desc)) {
      if (thread->file->accessors[i] == thread)
        result = false;
      else
        shutdown(thread->file->accessors[i]->client_desc, SHUT_RDWR);
    }

    pthread_mutex_unlock(&thread->file->accessors[i]->mutex);
  }

  return result;
}

bool open_file(thread_data *thread, string file_name) {
  response rsp;

  if (thread->file != NULL) {
    pthread_mutex_lock(&thread->mutex);
    rsp.type = ERROR_ALREADY_ACCESSING_A_FILE;
    bool result = send_response(rsp, thread->client_desc);
    pthread_mutex_unlock(&thread->mutex);
    return result;
  } else {
    pthread_mutex_lock(&thread->shared->mutex);

    if (file_exists_in_db(thread->shared->db, file_name.data)) {
      file_data *found = NULL;
      file_data_list *end = NULL;
      for (file_data_list *i = thread->shared->file_list; i != NULL;
           i = i->next) {
        end = i;
        if (strcmp(i->node->file, file_name.data) == 0) {
          found = i->node;
          break;
        }
      }

      if (found == NULL) {
        file_data_list *new;
        if (end == NULL) {
          thread->shared->file_list = malloc(sizeof(file_data_list));
          new = thread->shared->file_list;
        } else {
          end->next = malloc(sizeof(file_data_list));
          new = end->next;
        }
        new->next = NULL;
        new->node = malloc(sizeof(file_data));
        new->node->file = malloc(file_name.size);
        strcpy(new->node->file, file_name.data);
        get_file(thread->shared->db, file_name.data, &new->node->content,
                 &new->node->language);
        pthread_mutex_init(&new->node->mutex, NULL);
        new->node->accessors[0] = NULL;
        new->node->accessors[1] = NULL;
        found = new->node;
      }
      pthread_mutex_lock(&found->mutex);

      size_t position = 0;
      while (found->accessors[position] != NULL)
        ++position;

      if (position < 2) {
        thread->file = found;
        found->accessors[position] = thread;
        found->pos[position].line = 0;
        found->pos[position].column = 0;
        found->pos_end[position].line = 0;
        found->pos_end[position].column = 0;
        rsp.type = UPDATE_OPENED_FILE;
        rsp.id = thread->id;
        rsp.str = file_name;
        bool result = send_response_to_everyone(rsp, thread);

        if (!result) {
          pthread_mutex_unlock(&thread->file->mutex);
          pthread_mutex_unlock(&thread->shared->mutex);
          return false;
        }

        pthread_mutex_unlock(&thread->shared->mutex);

        rsp.type = UPDATE_INSERT;
        rsp.id = thread->id;
        rsp.pos.line = 0;
        rsp.pos.column = 0;
        rsp.str.data = thread->file->content;
        rsp.str.size = strlen(rsp.str.data) + 1;
        result = send_response(rsp, thread->client_desc);
        if (!result) {
          pthread_mutex_unlock(&thread->file->mutex);
          return false;
        }

        rsp.type = UPDATE_CHANGED_FILE_LANGUAGE;
        if (thread->file->language == NULL)
          rsp.str.size = 0;
        else
          rsp.str.size = strlen(thread->file->language) + 1;
        rsp.str.data = thread->file->language;
        result = send_response(rsp, thread->client_desc);
        if (!result) {
          pthread_mutex_unlock(&thread->file->mutex);
          return false;
        }

        for (int i = 0; i < 2; ++i) {
          if (thread->file->accessors[i] != NULL &&
              thread->file->accessors[i] != thread) {
            rsp.type = UPDATE_CHANGE_POSITION;
            rsp.id = thread->file->accessors[i]->id;
            rsp.pos = thread->file->pos[i];
            rsp.pos_end = thread->file->pos_end[i];
            result = send_response(rsp, thread->client_desc);
            if (!result) {
              pthread_mutex_unlock(&thread->file->mutex);
              return false;
            }
          }
        }

        pthread_mutex_unlock(&thread->file->mutex);

        return result;
      } else {
        pthread_mutex_lock(&thread->mutex);
        pthread_mutex_unlock(&thread->shared->mutex);
        rsp.type = ERROR_OPENED_FILE_HAS_TOO_MANY_USERS;
        bool result = send_response(rsp, thread->client_desc);
        pthread_mutex_unlock(&thread->mutex);
        return result;
      }
    } else {
      pthread_mutex_lock(&thread->mutex);
      pthread_mutex_unlock(&thread->shared->mutex);
      rsp.type = ERROR_OPENED_FILE_DOES_NOT_EXIST;
      bool result = send_response(rsp, thread->client_desc);
      pthread_mutex_unlock(&thread->mutex);
      return result;
    }
  }
}

void remove_and_save_file(thread_data *thread) {
  if (thread->file->accessors[0] == thread)
    thread->file->accessors[0] = NULL;
  else
    thread->file->accessors[1] = NULL;

  if (thread->file->accessors[0] == NULL &&
      thread->file->accessors[1] == NULL) {
    update_file(thread->shared->db, thread->file->file, thread->file->content,
                thread->file->language);
    if (thread->shared->file_list->node != thread->file) {
      for (file_data_list *i = thread->shared->file_list; i->next != NULL;
           i = i->next)
        if (i->next->node == thread->file) {
          i->next = i->next->next;
          break;
        }
    } else {
      thread->shared->file_list = thread->shared->file_list->next;
    }

    free(thread->file->content);
    free(thread->file);
  }
}

bool close_file(thread_data *thread) {
  response rsp;
  if (thread->file == NULL) {
    pthread_mutex_lock(&thread->mutex);
    rsp.type = ERROR_NO_FILE_TO_CLOSE;
    bool result = send_response(rsp, thread->client_desc);
    pthread_mutex_unlock(&thread->mutex);
    return result;
  }

  pthread_mutex_lock(&thread->shared->mutex);
  pthread_mutex_lock(&thread->file->mutex);
  rsp.type = UPDATE_CLOSED_FILE;
  rsp.id = thread->id;
  rsp.str.size = strlen(thread->file->file) + 1;
  rsp.str.data = malloc(rsp.str.size);
  strcpy(rsp.str.data, thread->file->file);

  remove_and_save_file(thread);

  bool result = send_response_to_everyone(rsp, thread);
  pthread_mutex_lock(&thread->mutex);
  pthread_mutex_unlock(&thread->file->mutex);
  pthread_mutex_unlock(&thread->shared->mutex);
  thread->file = NULL;
  pthread_mutex_unlock(&thread->mutex);
  free_response(rsp);
  return result;
}

bool create_file(thread_data *thread, string file_name) {
  response rsp;
  pthread_mutex_lock(&thread->shared->mutex);

  if (file_exists_in_db(thread->shared->db, file_name.data)) {
    pthread_mutex_lock(&thread->mutex);
    pthread_mutex_unlock(&thread->shared->mutex);
    rsp.type = ERROR_CREATED_FILE_ALREADY_EXISTS;
    bool result = send_response(rsp, thread->client_desc);
    pthread_mutex_unlock(&thread->mutex);
    return result;
  } else {
    create_file_in_db(thread->shared->db, file_name.data);
    rsp.type = UPDATE_ADDED_FILE;
    rsp.str = file_name;
    bool result = send_response_to_everyone(rsp, thread);
    pthread_mutex_unlock(&thread->shared->mutex);
    return result;
  }
}

bool file_in_use(thread_data *thread, string file_name) {
  bool result = false;

  for (thread_data_list *i = thread->shared->list; i != NULL; i = i->next) {
    pthread_mutex_lock(&i->node->mutex);

    if (i->node->file != NULL &&
        strcmp(file_name.data, i->node->file->file) == 0) {
      result = true;

      pthread_mutex_unlock(&i->node->mutex);

      break;
    }

    pthread_mutex_unlock(&i->node->mutex);
  }

  return result;
}

bool remove_file(thread_data *thread, string file_name) {
  response rsp;
  pthread_mutex_lock(&thread->shared->mutex);

  if (file_exists_in_db(thread->shared->db, file_name.data)) {
    if (file_in_use(thread, file_name)) {
      pthread_mutex_lock(&thread->mutex);
      pthread_mutex_unlock(&thread->shared->mutex);
      rsp.type = ERROR_REMOVED_FILE_IS_IN_USE;
      bool result = send_response(rsp, thread->client_desc);
      pthread_mutex_unlock(&thread->mutex);
      return result;
    } else {
      remove_file_from_db(thread->shared->db, file_name.data);
      rsp.type = UPDATE_REMOVED_FILE;
      rsp.str = file_name;
      bool result = send_response_to_everyone(rsp, thread);
      pthread_mutex_unlock(&thread->shared->mutex);
      return result;
    }
  } else {
    pthread_mutex_lock(&thread->mutex);
    pthread_mutex_unlock(&thread->shared->mutex);
    rsp.type = ERROR_REMOVED_FILE_DOES_NOT_EXIST;
    bool result = send_response(rsp, thread->client_desc);
    pthread_mutex_unlock(&thread->mutex);
    return result;
  }
}

bool change_file_language(thread_data *thread, string language) {
  response rsp;

  if (thread->file == NULL) {
    rsp.type = ERROR_NO_FILE_TO_EDIT;
    pthread_mutex_lock(&thread->mutex);
    bool result = send_response(rsp, thread->client_desc);
    pthread_mutex_unlock(&thread->mutex);
    return result;
  } else {
    pthread_mutex_lock(&thread->file->mutex);
    if (language.size > 0) {
      thread->file->language = realloc(thread->file->language, language.size);
      strcpy(thread->file->language, language.data);
    } else {
      free(thread->file->language);
      thread->file->language = NULL;
    }
    rsp.type = UPDATE_CHANGED_FILE_LANGUAGE;
    rsp.str = language;
    bool result = send_response_to_clients_accessing_same_file(rsp, thread);
    pthread_mutex_unlock(&thread->file->mutex);
    return result;
  }
}

size_t get_actual_position(char *content, position *pos) {
  char *line_begin;
  if (pos->line <= 0) {
    line_begin = content;
    pos->line = 0;
  } else {
    size_t i = 0;
    size_t line = 0;
    line_begin = content;
    while (content[i] != '\0') {
      if (content[i] == '\n') {
        ++line;
        line_begin = content + i + 1;
        if (line == pos->line)
          break;
      }
      ++i;
    }
    pos->line = line;
  }
  if (pos->column <= 0) {
    pos->column = 0;
    return line_begin - content;
  } else {
    size_t i = 0;
    while (line_begin[i] != '\0' && line_begin[i] != '\n') {
      if (i == pos->column)
        break;
      ++i;
    }
    pos->column = i;
    return line_begin + i - content;
  }
}

bool change_position(thread_data *thread, position pos, position pos_end) {
  response rsp;

  if (thread->file == NULL) {
    rsp.type = ERROR_NO_FILE_TO_EDIT;
    pthread_mutex_lock(&thread->mutex);
    bool result = send_response(rsp, thread->client_desc);
    pthread_mutex_unlock(&thread->mutex);
    return result;
  } else {
    pthread_mutex_lock(&thread->file->mutex);
    get_actual_position(thread->file->content, &pos);
    get_actual_position(thread->file->content, &pos_end);
    for (int i = 0; i < 2; ++i) {
      if (thread->file->accessors[i] == thread) {
        thread->file->pos[i] = pos;
        thread->file->pos_end[i] = pos_end;
        break;
      }
    }
    rsp.type = UPDATE_CHANGE_POSITION;
    rsp.id = thread->id;
    rsp.pos = pos;
    rsp.pos_end = pos_end;
    bool result = send_response_to_clients_accessing_same_file(rsp, thread);
    pthread_mutex_unlock(&thread->file->mutex);
    return result;
  }
}

bool insert(thread_data *thread, position pos, string str) {
  response rsp;

  if (thread->file == NULL) {
    rsp.type = ERROR_NO_FILE_TO_EDIT;
    pthread_mutex_lock(&thread->mutex);
    bool result = send_response(rsp, thread->client_desc);
    pthread_mutex_unlock(&thread->mutex);
    return result;
  } else {
    pthread_mutex_lock(&thread->file->mutex);
    rsp.type = UPDATE_INSERT;
    rsp.id = thread->id;
    rsp.pos = pos;
    rsp.str = str;
    size_t actual_pos = get_actual_position(thread->file->content, &rsp.pos);
    size_t content_size = strlen(thread->file->content);
    char *new = malloc(content_size + str.size + 1);
    strncpy(new, thread->file->content, actual_pos);
    new[actual_pos] = '\0';
    strcat(new, str.data);
    strcat(new, thread->file->content + actual_pos);
    free(thread->file->content);
    thread->file->content = new;
    bool result = send_response_to_clients_accessing_same_file(rsp, thread);
    pthread_mutex_unlock(&thread->file->mutex);
    return result;
  }
}

bool delete_range(thread_data *thread, position pos, position pos_end) {
  response rsp;

  if (thread->file == NULL) {
    rsp.type = ERROR_NO_FILE_TO_EDIT;
    pthread_mutex_lock(&thread->mutex);
    bool result = send_response(rsp, thread->client_desc);
    pthread_mutex_unlock(&thread->mutex);
    return result;
  } else {
    pthread_mutex_unlock(&thread->file->mutex);
    rsp.type = UPDATE_DELETE_RANGE;
    rsp.id = thread->id;
    rsp.pos = pos;
    rsp.pos_end = pos_end;
    strcpy(thread->file->content +
               get_actual_position(thread->file->content, &rsp.pos),
           thread->file->content +
               get_actual_position(thread->file->content, &rsp.pos_end));
    if (rsp.pos.line > rsp.pos_end.line ||
        (rsp.pos.line == rsp.pos_end.line &&
         rsp.pos.column > rsp.pos_end.column)) {
      position temp = rsp.pos;
      rsp.pos = rsp.pos_end;
      rsp.pos_end = temp;
    }
    bool result;
    if (rsp.pos.line == rsp.pos_end.line &&
        rsp.pos.column == rsp.pos_end.column)
      result = true;
    else
      result = send_response_to_clients_accessing_same_file(rsp, thread);
    pthread_mutex_unlock(&thread->file->mutex);
    return result;
  }
}

void remove_thread(thread_data *thread) {
  pthread_mutex_lock(&thread->shared->mutex);

  if (thread->shared->list->node->id == thread->id)
    thread->shared->list = thread->shared->list->next;
  else
    for (thread_data_list *i = thread->shared->list; i->next != NULL;
         i = i->next)
      if (thread->id == i->next->node->id) {
        thread_data_list *temp = i->next;
        i->next = i->next->next;
        free(temp);
        break;
      }

  if (thread->file != NULL) {
    pthread_mutex_lock(&thread->file->mutex);
    response rsp;
    rsp.type = UPDATE_CLOSED_FILE;
    rsp.id = thread->id;
    rsp.str.size = strlen(thread->file->file) + 1;
    rsp.str.data = malloc(rsp.str.size);
    strcpy(rsp.str.data, thread->file->file);

    remove_and_save_file(thread);

    pthread_mutex_unlock(&thread->file->mutex);
    send_response_to_everyone(rsp, thread);
    free_response(rsp);
  }

  pthread_mutex_unlock(&thread->shared->mutex);
}

bool add_thread(thread_data *thread) {
  if (!send_id(thread->id, thread->client_desc))
    return false;

  string *file_list;
  size_t file_list_size;

  pthread_mutex_lock(&thread->shared->mutex);

  get_file_list(thread->shared->db, &file_list, &file_list_size);

  for (size_t i = 0; i < file_list_size; ++i) {
    response rsp = {
        .type = UPDATE_ADDED_FILE,
        .str = file_list[i],
    };
    if (!send_response(rsp, thread->client_desc)) {
      pthread_mutex_unlock(&thread->shared->mutex);
      free_response(rsp);
      free(file_list);
      return false;
    }
    free_response(rsp);
  }
  free(file_list);

  for (thread_data_list *i = thread->shared->list; i != NULL; i = i->next)
    if (i->node->file != NULL) {
      pthread_mutex_lock(&i->node->file->mutex);
      response rsp;
      rsp.type = UPDATE_OPENED_FILE;
      rsp.id = i->node->id;
      rsp.str.size = strlen(i->node->file->file) + 1;
      rsp.str.data = malloc(rsp.str.size);
      strcpy(rsp.str.data, i->node->file->file);
      if (!send_response(rsp, thread->client_desc)) {
        pthread_mutex_unlock(&thread->shared->mutex);
        free_response(rsp);
        return false;
      }
      free_response(rsp);
      pthread_mutex_unlock(&i->node->file->mutex);
    }

  thread_data_list *new = malloc(sizeof(thread_data_list *));
  new->node = thread;
  new->next = thread->shared->list;
  thread->shared->list = new;

  pthread_mutex_unlock(&thread->shared->mutex);

  return true;
}
