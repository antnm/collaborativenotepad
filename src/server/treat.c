#include "treat.h"
#include "../message.h"
#include "thread.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

void *treat(void *arg) {
  thread_data *thread = (struct thread_data *)arg;

  printf("[thread %d] Waiting for message\n", thread->id);

  pthread_detach(pthread_self());

  if (!add_thread(thread)) {
    close(thread->client_desc);
    printf("[thread %d] Exiting\n", thread->id);
    free(thread);
    return NULL;
  }

  bool connected = true;

  while (connected) {
    request req;
    if (!recieve_request(&req, thread->client_desc))
      break;

    switch (req.type) {
    case REQUEST_OPEN_FILE:
      printf("[thread %d] Client request to open file %s\n", thread->id,
             req.str.data);
      connected = open_file(thread, req.str);
      break;

    case REQUEST_CLOSE_FILE:
      printf("[thread %d] Client request to close file\n", thread->id);
      connected = close_file(thread);
      break;

    case REQUEST_CREATE_FILE:
      printf("[thread %d] Client requested file %s to be created\n", thread->id,
             req.str.data);
      connected = create_file(thread, req.str);
      break;

    case REQUEST_REMOVE_FILE:
      printf("[thread %d] Client requested file %s to be removed\n", thread->id,
             req.str.data);
      connected = remove_file(thread, req.str);
      break;

    case REQUEST_CHANGE_FILE_LANGUAGE:
      printf("[thread %d] Client requested to change file language to %s\n", thread->id,
             req.str.data);
      connected = change_file_language(thread, req.str);
      break;

    case REQUEST_CHANGE_POSITION:
      printf("[thread %d] Client requested to change position to %d:%d-%d:%d\n",
             thread->id, req.pos.column, req.pos.line, req.pos_end.column,
             req.pos_end.line);
      connected = change_position(thread, req.pos, req.pos_end);
      break;

    case REQUEST_INSERT:
      printf("[thread %d] Client requested to insert text %s at %d:%d\n",
             thread->id, req.str.data, req.pos.column, req.pos.line);
      connected = insert(thread, req.pos, req.str);
      break;

    case REQUEST_DELETE_RANGE:
      printf("[thread %d] Client requested to delete %d:%d-%d:%d\n", thread->id,
             req.pos.column, req.pos.line, req.pos_end.column,
             req.pos_end.line);
      connected = delete_range(thread, req.pos, req.pos_end);
      break;
    }

    free_request(req);
  }

  remove_thread(thread);

  close(thread->client_desc);
  printf("[thread %d] Exiting\n", thread->id);
  free(thread);
  return NULL;
}
