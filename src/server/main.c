#include "database.h"
#include "thread.h"
#include "treat.h"
#include <errno.h>
#include <netinet/in.h>
#include <pthread.h>
#include <sqlite3.h>
#include <stdio.h>
#include <stdlib.h>

#define PORT 2908

int new_socket() {
  int sd;

  if ((sd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
    perror("socket() error");
  }
  int on = 1;
  setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on));

  struct sockaddr_in server = {.sin_family = AF_INET,
                               .sin_addr.s_addr = htonl(INADDR_ANY),
                               .sin_port = htons(PORT)};

  if (bind(sd, (struct sockaddr *)&server, sizeof(struct sockaddr)) == -1) {
    perror("bind() error");
  }

  if (listen(sd, 2) == -1) {
    perror("listen() error");
  }

  return sd;
}

int main() {
  int sd = new_socket();

  shared_thread_data shared;
  shared.db = new_db();
  shared.list = NULL;
  shared.file_list = NULL;
  pthread_mutex_init(&shared.mutex, NULL);

  uint32_t id = 0;
  while (1) {
    printf("Waiting at port %d...\n", PORT);

    int client;

    struct sockaddr_in from;
    socklen_t length = sizeof(from);
    if ((client = accept(sd, (struct sockaddr *)&from, &length)) < 0) {
      perror("accept() error");
      return errno;
    }

    struct thread_data *td = malloc(sizeof(thread_data));
    td->id = id++;
    td->client_desc = client;
    td->file = NULL;
    pthread_mutex_init(&td->mutex, NULL);
    td->shared = &shared;

    pthread_t thread;
    pthread_create(&thread, NULL, &treat, td);
  }

  sqlite3_close(shared.db);
}
