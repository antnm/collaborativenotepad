#include "message.h"
#include <assert.h>
#include <stdlib.h>
#include <sys/socket.h>

bool send_n_bytes(int desc, void *ptr, size_t n) {
  if (n > 0)
    return send(desc, ptr, n, MSG_NOSIGNAL) == n;
  ptr = NULL;
  return true;
}

bool recv_n_bytes(int desc, void *ptr, size_t n) {
  if (n > 0)
    return recv(desc, ptr, n, MSG_WAITALL) == n;
  ptr = NULL;
  return true;
}

bool send_id(uint32_t id, int desc) {
  return send_n_bytes(desc, &id, sizeof(id));
}

bool recieve_id(uint32_t *id, int desc) {
  return recv_n_bytes(desc, id, sizeof(*id));
}

bool send_request(request req, int desc) {
  if (!send_n_bytes(desc, &req.type, sizeof(req.type)))
    return false;

  switch (req.type) {
  case REQUEST_CREATE_FILE:
  case REQUEST_REMOVE_FILE:
  case REQUEST_OPEN_FILE:
  case REQUEST_CHANGE_FILE_LANGUAGE:
    return send_n_bytes(desc, &req.str.size, sizeof(req.str.size)) &&
           send_n_bytes(desc, req.str.data, req.str.size);

  case REQUEST_INSERT:
    return send_n_bytes(desc, &req.pos, sizeof(req.pos)) &&
           send_n_bytes(desc, &req.str.size, sizeof(req.str.size)) &&
           send_n_bytes(desc, req.str.data, req.str.size);

  case REQUEST_CHANGE_POSITION:
  case REQUEST_DELETE_RANGE:
    return send_n_bytes(desc, &req.pos, sizeof(req.pos)) &&
           send_n_bytes(desc, &req.pos_end, sizeof(req.pos_end));

  case REQUEST_CLOSE_FILE:
    return true;

  default:
    assert("Request type invalid" && false);
  }
}

bool recieve_request(request *req, int desc) {
  if (!recv_n_bytes(desc, &req->type, sizeof(req->type)))
    return false;

  switch (req->type) {
  case REQUEST_CREATE_FILE:
  case REQUEST_REMOVE_FILE:
  case REQUEST_OPEN_FILE:
  case REQUEST_CHANGE_FILE_LANGUAGE:
    if (!recv_n_bytes(desc, &req->str.size, sizeof(req->str.size)))
      return false;
    req->str.data = malloc(req->str.size);
    if (!recv_n_bytes(desc, req->str.data, req->str.size)) {
      free(req->str.data);
      return false;
    }
    return true;

  case REQUEST_INSERT:
    if (!recv_n_bytes(desc, &req->pos, sizeof(req->pos)) ||
        !recv_n_bytes(desc, &req->str.size, sizeof(req->str.size)))
      return false;
    req->str.data = malloc(req->str.size);
    if (!recv_n_bytes(desc, req->str.data, req->str.size)) {
      free(req->str.data);
      return false;
    }
    return true;

  case REQUEST_CHANGE_POSITION:
  case REQUEST_DELETE_RANGE:
    return recv_n_bytes(desc, &req->pos, sizeof(req->pos)) &&
           recv_n_bytes(desc, &req->pos_end, sizeof(req->pos_end));

  case REQUEST_CLOSE_FILE:
    return true;
  default:
    return false;
  }
}

void free_request(request req) {
  switch (req.type) {

  case REQUEST_CREATE_FILE:
  case REQUEST_REMOVE_FILE:
  case REQUEST_OPEN_FILE:
  case REQUEST_INSERT:
  case REQUEST_CHANGE_FILE_LANGUAGE:
    free(req.str.data);
    break;

  case REQUEST_DELETE_RANGE:
  case REQUEST_CHANGE_POSITION:
  case REQUEST_CLOSE_FILE:
    break;

  default:
    assert("Request type invalid" && false);
  }
}

bool send_response(response rsp, int desc) {
  if (!send_n_bytes(desc, &rsp.type, sizeof(rsp.type)))
    return false;

  switch (rsp.type) {
  case UPDATE_ADDED_FILE:
  case UPDATE_REMOVED_FILE:
  case UPDATE_CHANGED_FILE_LANGUAGE:
    return send_n_bytes(desc, &rsp.str.size, sizeof(rsp.str.size)) &&
           send_n_bytes(desc, rsp.str.data, rsp.str.size);

  case UPDATE_CLOSED_FILE:
  case UPDATE_OPENED_FILE:
    return send_n_bytes(desc, &rsp.id, sizeof(rsp.id)) &&
           send_n_bytes(desc, &rsp.str.size, sizeof(rsp.str.size)) &&
           send_n_bytes(desc, rsp.str.data, rsp.str.size);

  case UPDATE_INSERT:
    return send_n_bytes(desc, &rsp.id, sizeof(rsp.id)) &&
           send_n_bytes(desc, &rsp.pos, sizeof(rsp.pos)) &&
           send_n_bytes(desc, &rsp.str.size, sizeof(rsp.str.size)) &&
           send_n_bytes(desc, rsp.str.data, rsp.str.size);

  case UPDATE_DELETE_RANGE:
  case UPDATE_CHANGE_POSITION:
    return send_n_bytes(desc, &rsp.id, sizeof(rsp.id)) &&
           send_n_bytes(desc, &rsp.pos, sizeof(rsp.pos)) &&
           send_n_bytes(desc, &rsp.pos_end, sizeof(rsp.pos_end));

  case ERROR_OPENED_FILE_DOES_NOT_EXIST:
  case ERROR_OPENED_FILE_HAS_TOO_MANY_USERS:
  case ERROR_NO_FILE_TO_CLOSE:
  case ERROR_CREATED_FILE_ALREADY_EXISTS:
  case ERROR_REMOVED_FILE_DOES_NOT_EXIST:
  case ERROR_REMOVED_FILE_IS_IN_USE:
  case ERROR_ALREADY_ACCESSING_A_FILE:
  case ERROR_NO_FILE_TO_EDIT:
    return true;
  default:
    assert("Response type invalid" && false);
  }
}

bool recieve_response(response *rsp, int desc) {
  if (!recv_n_bytes(desc, &rsp->type, sizeof(rsp->type)))
    return false;
  switch (rsp->type) {
  case UPDATE_ADDED_FILE:
  case UPDATE_REMOVED_FILE:
  case UPDATE_CHANGED_FILE_LANGUAGE:
    if (!recv_n_bytes(desc, &rsp->str.size, sizeof(rsp->str.size)))
      return false;
    rsp->str.data = malloc(rsp->str.size);
    if (!recv_n_bytes(desc, rsp->str.data, rsp->str.size)) {
      free(rsp->str.data);
      return false;
    }
    return true;

  case UPDATE_CLOSED_FILE:
  case UPDATE_OPENED_FILE:
    if (!recv_n_bytes(desc, &rsp->id, sizeof(rsp->id)) ||
        !recv_n_bytes(desc, &rsp->str.size, sizeof(rsp->str.size)))
      return false;
    rsp->str.data = malloc(rsp->str.size);
    if (!recv_n_bytes(desc, rsp->str.data, rsp->str.size)) {
      free(rsp->str.data);
      return false;
    }
    return true;

  case UPDATE_INSERT:
    if (!recv_n_bytes(desc, &rsp->id, sizeof(rsp->id)) ||
        !recv_n_bytes(desc, &rsp->pos, sizeof(rsp->pos)) ||
        !recv_n_bytes(desc, &rsp->str.size, sizeof(rsp->str.size)))
      return false;
    rsp->str.data = malloc(rsp->str.size);
    if (!recv_n_bytes(desc, rsp->str.data, rsp->str.size)) {
      free(rsp->str.data);
      return false;
    }
    return true;

  case UPDATE_DELETE_RANGE:
  case UPDATE_CHANGE_POSITION:
    return recv_n_bytes(desc, &rsp->id, sizeof(rsp->id)) &&
           recv_n_bytes(desc, &rsp->pos, sizeof(rsp->pos)) &&
           recv_n_bytes(desc, &rsp->pos_end, sizeof(rsp->pos_end));

  case ERROR_OPENED_FILE_DOES_NOT_EXIST:
  case ERROR_OPENED_FILE_HAS_TOO_MANY_USERS:
  case ERROR_NO_FILE_TO_CLOSE:
  case ERROR_CREATED_FILE_ALREADY_EXISTS:
  case ERROR_REMOVED_FILE_DOES_NOT_EXIST:
  case ERROR_REMOVED_FILE_IS_IN_USE:
  case ERROR_ALREADY_ACCESSING_A_FILE:
  case ERROR_NO_FILE_TO_EDIT:
    return true;

  default:
    assert("Response type invalid" && false);
  }
}

void free_response(response rsp) {
  switch (rsp.type) {
  case UPDATE_ADDED_FILE:
  case UPDATE_CLOSED_FILE:
  case UPDATE_CHANGED_FILE_LANGUAGE:
  case UPDATE_REMOVED_FILE:
  case UPDATE_OPENED_FILE:
  case UPDATE_INSERT:
    free(rsp.str.data);
    break;

  case UPDATE_CHANGE_POSITION:
  case UPDATE_DELETE_RANGE:
  case ERROR_OPENED_FILE_DOES_NOT_EXIST:
  case ERROR_OPENED_FILE_HAS_TOO_MANY_USERS:
  case ERROR_NO_FILE_TO_CLOSE:
  case ERROR_CREATED_FILE_ALREADY_EXISTS:
  case ERROR_REMOVED_FILE_DOES_NOT_EXIST:
  case ERROR_REMOVED_FILE_IS_IN_USE:
  case ERROR_ALREADY_ACCESSING_A_FILE:
  case ERROR_NO_FILE_TO_EDIT:
    break;

  default:
    assert("Response type invalid" && false);
  }
}
